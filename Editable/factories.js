// Use _factories
//Use _factories
_factories.factory('ConfigSvc', function($resource, API_ENDPOINT_CUSTOM){
	return {
		webconfig : function () {
			return $resource(API_ENDPOINT_CUSTOM.config +'/webconfig',{},{
				get: {
					method: 'POST',
					headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
					params: {
					},
					data : {}
				}
			});   
		}  
	}
});