<?php
error_reporting(0);
//session_start();
pg_query($link, "DEALLOCATE ALL");

		
pg_prepare($link,'sql5b','SELECT * from w_business WHERE id=$1');
$result23b = pg_execute($link,'sql5b',array($bus_id));
$row23b = pg_fetch_array($result23b);
$sitecurrency = currency_symbol($row23b['currency']);
$deliverytime = $row23b['deliverytime'];
$pickuptime = $row23b['pickuptime'];
$bstreet = $row23b['street'];
$bcolony = $row23b['colony'];
$btel = $row23b['tel'];						 


//$serverlink = 'http://'.$_SERVER['HTTP_HOST'];
$serverlink = $sitename;

$msgRest =<<<END
<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<body style="font-family: 'Open Sans', sans-serif !important; width: 100%; float: left; background: #eee; margin: 0; padding: 0;" bgcolor="#eee">
<style type="text/css">
@font-face {
font-family: 'Open Sans'; font-style: normal; font-weight: 300; src: local('Open Sans Light'), local('OpenSans-Light'), url('https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTYnF5uFdDttMLvmWuJdhhgs.ttf') format('truetype');
}
@font-face {
font-family: 'Open Sans'; font-style: normal; font-weight: 400; src: local('Open Sans'), local('OpenSans'), url('https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf') format('truetype');
}
@font-face {
font-family: 'Open Sans'; font-style: normal; font-weight: 600; src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url('https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSonF5uFdDttMLvmWuJdhhgs.ttf') format('truetype');
}
@font-face {
font-family: 'Open Sans'; font-style: normal; font-weight: 700; src: local('Open Sans Bold'), local('OpenSans-Bold'), url('https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzInF5uFdDttMLvmWuJdhhgs.ttf') format('truetype');
}
@font-face {
font-family: 'Open Sans'; font-style: normal; font-weight: 800; src: local('Open Sans Extrabold'), local('OpenSans-Extrabold'), url('https://fonts.gstatic.com/s/opensans/v13/EInbV5DfGHOiMmvb1Xr-honF5uFdDttMLvmWuJdhhgs.ttf') format('truetype');
}
.track_my_order_now_btn:hover {
background: #c42424; border-bottom: 3px solid #a71616;
}
.drvr-btn:hover {
background: #0ca775;
}
.ordr-btn:hover {
background: #0b2878;
}
.cncl-btn:hover {
background: #df2222;
}
@media (max-width: 767px) {
  .table-responsive {
    width: 100%; margin-bottom: 15px; overflow-y: hidden; -ms-overflow-style: -ms-autohiding-scrollbar; border: 1px solid #ddd;
  }
  .row li {
    float: left; width: 100%; padding-right: 0px;
  }
  .template_wrapper {
    padding: 20px;
  }
  .templet-body {
    padding: 15px;
  }
  .drvr-btn {
    width: 100%; margin: 0px; font-size: 11px;
  }
  .drvr-btn {
    width: 100%; margin: 15px 0px 0px 0px; font-size: 11px;
  }
  .ordr-btn {
    width: 100%; margin: 15px 0px 0px 0px; font-size: 11px;
  }
  .cncl-btn {
    width: 100%; margin: 15px 0px 0px 0px; font-size: 11px;
  }
}
@media (max-width: 600px) {
  .track_my_order_now_btn {
    width: 100%;
  }
}
</style>
	<div class="header" style="height: 256px; width: 100%; float: left; margin-bottom: 66px; background: #121c25; padding: 25px 0px;">
		<h1 style="color: #fff; font-size: 35px; text-align: center; margin: 80px 0px 0px;" align="center">{$lang_resource['ORDERING_HEADING']}</h1>
	</div>
	<div class="template_wrapper" style="width: 85%; clear: both; background: #fff; margin: 0px auto; padding: 7% 0%;">
				<div class="logo" style="width: 100%; height: 50px; text-align: center; margin-bottom: 50px;" align="center">
</div>
END;
if($data->buyer->deliveryType == "delivery"){
	if($deliverytime == "" || $deliverytime == "undefined"){
		$d_time = '00:00';							
	}else{
		$d_time = $deliverytime;				
	}
	$est_time = $lang_resource['ESTIMATE_DELIVERY_TIME'];
}else{
	if($pickuptime == "" || $pickuptime == "undefined"){
		$d_time = '00:00';							
	}else{
		$d_time = $pickuptime;				
	}
	$est_time = $lang_resource['ESTIMATE_PICKUP_TIME'];	
}
$msgRest .=<<<END
		<div class="templet-header-red" style="color: #fff; text-align: center; background: #e64949; padding: 15px;" align="center">
			<h4 style="font-weight: 300; font-size: 18px; margin: 0px;">$est_time</h4>
			<h1 style="font-weight: 400; font-size: 60px; margin: 5px 0px;">$d_time</h1>
		</div>
		<div class="templet-body" style="padding: 8%; border: 1px solid #eaeaea;">
			<h4 class="order_no_heading" style="font-weight: 400; font-size: 22px; color: #484848; text-transform: uppercase; margin: 10px 0px; padding: 0px 15px;">{$lang_resource['CONTROL_PANEL_MENU_ORDERS']} # $order->id</h4>
			<div class="product_details" style="padding: 0px 15px 30px; margin: 0px 0px 20px; border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: #cecece; display: table; content: ' '; width: 100%;">
				<ul class="row" style="margin: 0; padding: 0; list-style: none;">
<li style="float: left; width: 45%;">
						<h4 style="font-weight: 400; font-size: 18px; color: #484848; margin: 10px 0px;">{$lang_resource['FROM_HEADING']}</h4>
						<p style="color: #616161; font-size: 16px; text-align: left; font-weight: 400; margin: 2px 0px;" align="left"><strong>{$data->business[0]->name}</strong>
						</p>
						<p style="color: #616161; font-size: 16px; text-align: left; font-weight: 400; margin: 2px 0px;" align="left">$bstreet</p>
						<p style="color: #616161; font-size: 16px; text-align: left; font-weight: 400; margin: 2px 0px;" align="left">$bcolony</p>
						<p style="color: #616161; font-size: 16px; text-align: left; font-weight: 400; margin: 2px 0px;" align="left">$btel</p>
					</li>
					<li style="float: left; width: 45%;">
						<h4 style="font-weight: 400; font-size: 18px; color: #484848; margin: 10px 0px;">{$lang_resource['TO_HEADING']}</h4>
						<p style="color: #616161; font-size: 16px; text-align: left; font-weight: 400; margin: 2px 0px;" align="left"><strong>{$data->buyer->name}</strong>
						</p>
						<p style="color: #616161; font-size: 16px; text-align: left; font-weight: 400; margin: 2px 0px;" align="left">{$data->buyer->address}</p>
						<p style="color: #616161; font-size: 16px; text-align: left; font-weight: 400; margin: 2px 0px;" align="left">{$data->buyer->colony}</p>
						<p style="color: #616161; font-size: 16px; text-align: left; font-weight: 400; margin: 2px 0px;" align="left">{$data->buyer->tel}</p>
					</li>
				</ul>
</div>
END;
$twilio_phone;
$twilio_enabled;
$twilio_order = "";
$business = $data->business[0];
$twilio_phone = $business->twiliophone;
//$twilio_enabled = $business->twilioenabled;

pg_prepare($link,'sql4','SELECT buys from w_business WHERE id=$1');
$result2 = pg_execute($link,'sql4',array($business->id));
if (pg_num_rows($result2)==1)  
	while($row2 = pg_fetch_array($result2)){
		pg_prepare($link,'sqls','UPDATE w_business SET buys=$2 WHERE id=$1');
		pg_execute($link,'sqls',array($business->id,intval($row2['buys'])+1));
	}

$total = 0;
$msgRest .=<<<END
			<div class="product_details_tbl" style="margin: 0px; padding: 0px 0px 30px;">
				<div class="">
					<div class="table-responsive" style="min-height: .01%; overflow-x: auto;">
						<table class="confirmation_tbl order_dts_tbl" border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%;"><tbody>
END;
foreach ($business->dishes as $dish){
	if($dish->options) {
		$productOptionHtml =  Margeslash($dish->options);  
	} else {
		$productOptionHtml ='';
	}
	$dishcomments = str_replace("%20", " ", ucfirst($dish->comments));
$msgRest .=<<<END
<tr>
<td style="color: #616161; font-size: 13px; vertical-align: top; padding: 5px; border: none;" valign="top">$dish->quantity x</td>
									<td style="color: #616161; font-size: 13px; vertical-align: top; padding: 5px; border: none;" valign="top">$dish->name
										<br>$productOptionHtml
										<br>$dishcomments</td>
									<td style="color: #616161; font-size: 13px; vertical-align: top; text-align: right; padding: 5px; border: none;" align="right" valign="top">$sitecurrency $dish->total</td>
								</tr>
END;
$total = $total + $dish->total;
}
$msgRest .=<<<END
</tbody></table>
</div>
				</div>
			</div>
			<div class="product_details_tbl" style="margin: 0px; padding: 0px 0px 30px;">
				<div class="">
					<div class="table-responsive" style="min-height: .01%; overflow-x: auto;">
						<table class="confirmation_tbl order_dts_tbl" border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%;"><tbody>
END;
if($data->buyer->taxtype == 1){
	$total = $total + $business->shipping + $data->tax;
	$taxstring = $lang_resource['FRONT_MAIN_EMAIL_TAX_NOT_INCLUDED'];
}else{
	$total = $total + $business->shipping;
	$taxstring = $lang_resource['FRONT_MAIN_EMAIL_TAX_INCLUDED'];
}
$taxpercentage = GetDecimalPoint($data->buyer->tax);

$tipsprice = GetDecimalPoint($data->buyer->tips);

if ($data->buyer->tips > 0){
	$total = $total	+ $data->buyer->tips;
}
$total = GetDecimalPoint($total);
$deltype = $data->buyer->deliveryType;

if ($business->shipping=='0.00')
	$shippingcaption = $lang_resource['FRONT_MAIN_HOME_DELIVERY'];
else
	$shippingcaption = $lang_resource['FRONT_MAIN_HOME_DELIVERY_COST'];

$ordercmts = str_replace("%20", " ", ucfirst($order->buyer->comments));
$msgRest .=<<<END
<tr>
<td colspan="2" style="color: #616161; font-size: 13px; vertical-align: top; padding: 5px; border: none;" valign="top">$shippingcaption<small>$ordercmts</small></td>
									<td style="color: #616161; font-size: 13px; vertical-align: top; text-align: right; padding: 5px; border: none;" align="right" valign="top">
<strong>$sitecurrency $business->shipping</strong>
									</td>
								</tr>
END;
if($data->discountprice !=0 && isset($data->discountcategory)){
	if($data->discounttype == 1)
		$discountcaption = $lang_resource['SHOPPING_DISCOUNT_TEXT'] ." (" .$data->discountrate ."%)";
	else if($data->discounttype == 2)
		$discountcaption = $lang_resource['SHOPPING_DISCOUNT_TEXT'] ;

	$total = $total - $data->discountprice;
	$total = GetDecimalPoint($total);

	//$discmmnts = ucfirst(strtolower($data->discountcomments));
$msgRest .=<<<END
<tr>
<td colspan="2" style="color: #616161; font-size: 13px; vertical-align: top; padding: 5px; border: none;" valign="top">$discountcaption<small></small></td>
									<td style="color: #616161; font-size: 13px; vertical-align: top; text-align: right; padding: 5px; border: none;" align="right" valign="top">
<strong>$sitecurrency $data->discountprice</strong>
									</td>
								</tr>
END;
}
if($data->tax > 0){
$msgRest .=<<<END
<tr>
<td colspan="2" style="color: #616161; font-size: 13px; vertical-align: top; padding: 5px; border: none;" valign="top">{$lang_resource['Tax_V2']}</td>
									<td style="color: #616161; font-size: 13px; vertical-align: top; text-align: right; padding: 5px; border: none;" align="right" valign="top">
<strong>$sitecurrency $data->tax</strong>
									</td>
								</tr>
END;
}
if($data->buyer->tips > 0){
$txstr = ucfirst(strtolower($taxstring));
$msgRest .=<<<END
<tr>
<td colspan="2" style="color: #616161; font-size: 13px; vertical-align: top; padding: 5px; border: none;" valign="top">{$lang_resource['DELIVERY_TIP_DRIVER']}($taxpercentage %)<small>$txstr</small></td>
									<td class="credit_price_text" style="color: #616161; font-size: 13px; vertical-align: top; text-align: right; padding: 5px; border: none;" align="right" valign="top">
<strong>$sitecurrency $tipsprice</strong>
									</td>
								</tr>
END;
}

if ($data->business[0]->paymethod->transactium==true) {
				if ($paymethod=='')
				   if ($paymentid=='Failure'){
					$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_NOT_TRANSACTIUM'];
					}else{
					$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_TRANSACTIUM']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";	
					}
					
					
			}
			
if ($data->business[0]->paymethod->pexpress==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYMENTEXPRESS']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";		
}

if ($data->business[0]->paymethod->maksekeskus==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_MAKSEKESKUS']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";		
}

if ($data->business[0]->paymethod->voguepay==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_VOGUEPAY']." "."(" . $paymentid . ")";	
}

if ($data->business[0]->paymethod->skrill==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_SKRILL']." "."(" . $paymentid . ")";	
}

if ($data->business[0]->paymethod->payeezy==true){
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYEEZY']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->payu==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYU']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->stripe==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_STRIPE']." "."(" . $paymentid . ")";	
}
/*if ($data->business[0]->paymethod->btrans==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_BTRANS']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->bsa==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_BSA']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->azul==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['PAYMENT_GATEWAY_ALL_AZUL_PAYMENT_SUCCESS']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->quickpay==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_QUICKPAY']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->paynl==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYNL']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->zaakpay==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_ZAAKPAY']." "."(" . $paymentid . ")";	
}*/
if ($data->business[0]->paymethod->cash==true) {
	if ($paymethod=='')							
		$paymethod = $lang_resource['FRONT_MAIN_EMAIL_UPON_RECEIVING_PAY_CASH'];
}
if ($business->paymethod->card==true)
	if ($paymethod=='')
		$paymethod = $lang_resource['FRONT_MAIN_EMAIL_UPON_RECEIVING_PAY_CASH'];
	else
		$paymethod .= $lang_resource['AND_CARD'];
$msgRest .=<<<END
<tr>
<td colspan="2" class=" total_text" style="color: #616161 !important; font-size: 22px !important; vertical-align: top; padding: 5px; border: none;" valign="top">{$lang_resource['DELIVERY_TOTAL']}</td>
									<td class="total_price_text" style="color: #e64949 !important; font-size: 22px !important; vertical-align: top; text-align: right; padding: 5px; border: none;" align="right" valign="top">
<strong>$sitecurrency $data->total</strong>
									</td>
								</tr>
</tbody></table>
</div>
				</div>
			</div>
			<br><br><h4 class="method_text" style="font-size: 18px; color: #484848; text-align: center; font-weight: 400; margin: 10px 0px; padding: 0px;" align="center">{$lang_resource['FRONT_MAIN_PAYMENT_METHOD']} $paymethod</h4>
			<h4 class="method_text" style="font-size: 18px; color: #484848; text-align: center; font-weight: 400; margin: 10px 0px; padding: 0px;" align="center">{$lang_resource['DELIVERY_TYPE']} : $deltype</h4>
		</div>
		<p class="status" style="color: #e64949; font-size: 18px; text-align: center; margin: 35px 0px 10px;" align="center"><strong>{$lang_resource['EMAIL_TEMPLATE_PLEASE_CHOOSE_ONE_BELOW_OPTIONS']} $order->id</strong>
		</p>
		<button type="button" class="drvr-btn" style="width: 80%; height: 40px; color: #fff; text-align: center; font-size: 16px; font-weight: 600; border-radius: 3px; cursor: pointer; background: #37b98f; margin: 15px 10% 0; border: none;"><a href="$confirm_url15" style="color: #FFF; text-decoration: none;">{$lang_resource['ACCEPT_DELIVER_IN_15_MINUTES']}</a></button>
		<button type="button" class="drvr-btn" style="width: 80%; height: 40px; color: #fff; text-align: center; font-size: 16px; font-weight: 600; border-radius: 3px; cursor: pointer; background: #37b98f; margin: 15px 10% 0; border: none;"><a href="$confirm_url30" style="color: #FFF; text-decoration: none;">{$lang_resource['ACCEPT_DELIVER_IN_30_MINUTES']}</a></button>
		<button type="button" class="drvr-btn" style="width: 80%; height: 40px; color: #fff; text-align: center; font-size: 16px; font-weight: 600; border-radius: 3px; cursor: pointer; background: #37b98f; margin: 15px 10% 0; border: none;"><a href="$confirm_url45" style="color: #FFF; text-decoration: none;">{$lang_resource['ACCEPT_DELIVER_IN_45_MINUTES']}</a></button>
		<button type="button" class="drvr-btn" style="width: 80%; height: 40px; color: #fff; text-align: center; font-size: 16px; font-weight: 600; border-radius: 3px; cursor: pointer; background: #37b98f; margin: 15px 10% 0; border: none;"><a href="$confirm_url60" style="color: #FFF; text-decoration: none;">{$lang_resource['ACCEPT_DELIVER_IN_60_MINUTES']}</a></button>
		<button type="button" class="ordr-btn" style="width: 80%; height: 40px; color: #fff; text-align: center; font-size: 16px; font-weight: 600; border-radius: 3px; cursor: pointer; background: #234195; margin: 15px 10% 0; border: none;"><a href="$confirm_url" style="color: #FFF; text-decoration: none;">{$lang_resource['DRIVER_ORDER_DELIVERED']}</a></button>
		<button type="button" class="cncl-btn" style="width: 80%; height: 40px; color: #fff; text-align: center; font-size: 16px; font-weight: 600; border-radius: 3px; cursor: pointer; background: #e64949; margin: 15px 10% 0; border: none;"><a href="$reject_url" style="color: #FFF; text-decoration: none;">{$lang_resource['DRIVER_ORDER_CANCEL']}</a></button>
		<p class="status" style="color: #e64949; font-size: 18px; text-align: center; margin: 35px 0px 10px;" align="center"><strong>{$lang_resource['ORDER_EMAIL_DRIVER_RESTAURANT_COMMENTS']} : $order->comment</strong>
		</p>
	</div>
	<div class="template_footer" style="height: 135px; width: 100%; float: left; margin-top: 66px; background: #121c25; padding: 25px 0px;">
    <div class="footer_social" style="width: 200px; margin: 0px auto;">
      <ul style="width: 100%; margin: 0px; padding: 0; list-style: none;">
      </ul>
    </div>
    <p style="color: #c8c8c8; font-size: 16px; text-align: center; clear: both; margin-top: 20px;" align="center"></p>
  </div>
</body>
</html>
END;
?>
