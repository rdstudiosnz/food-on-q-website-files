<?php
//error_reporting(0);
///session_start();
$curyear=  date('Y'); 
//$serverlink = 'http://'.$_SERVER['HTTP_HOST'];
$serverlink = $sitename;
$msg = '<html>
    <head>
    <meta charset="utf-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Forcing initial-scale shouldnt be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Use the latest (edge) version of IE rendering engine -->
    <title>EmailTemplate-Responsive</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"> 
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
    <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->

    <!-- CSS Reset -->
    <style type="text/css">
/* What it does: Remove spaces around the email design added by some email clients. */
      /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
html,  body {
	margin: 0 !important;
	padding: 0 !important;
	height: 100% !important;
	width: 100% !important;
	font-family: Open Sans, sans-serif !important;
}
/* What it does: Stops email clients resizing small text. */
* {
	-ms-text-size-adjust: 100%;
	-webkit-text-size-adjust: 100%;
}
/* What it does: Forces Outlook.com to display emails full width. */
.ExternalClass {
	width: 100%;
}
/* What is does: Centers email on Android 4.4 */
div[style*="margin: 16px 0"] {
	margin: 0 !important;
}
/* What it does: Stops Outlook from adding extra spacing to tables. */
table,  td {
	mso-table-lspace: 0pt !important;
	mso-table-rspace: 0pt !important;
}
/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
table {
	border-spacing: 0 !important;
	border-collapse: collapse !important;
	table-layout: auto !important;
	margin: 0 auto !important;
}
table table table {
	table-layout: auto;
}
/* What it does: Uses a better rendering method when resizing images in IE. */
img {
	-ms-interpolation-mode: bicubic;
}
/* What it does: Overrides styles added when Yahoos auto-senses a link. */
.yshortcuts a {
	border-bottom: none !important;
}
/* What it does: Another work-around for iOS meddling in triggered links. */
a[x-apple-data-detectors] {
	color: inherit !important;
}
</style>

    <!-- Progressive Enhancements -->
    <style type="text/css">
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #d7094f !important;
            border-color: #d7094f !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid-centered {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* And center justify these ones. */
            .fluid-centered {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
        
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
                
        }

    </style>
    </head>
    <body bgcolor="#fff" width="100%" style="margin: 0;" yahoo="yahoo">
    <table bgcolor="#fff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse; ">
      <tr>
        <td><center style="width: 100%;">
             <!-- Email Header : BEGIN -->
            <table align="center" width="600" class="email-container" style="border-bottom:1px solid #dedede;">
            <tr>
                <td style="padding: 20px 0; text-align: center"><a href=""><img src="'.$logo->secure_url.'"  border="0" style="width:317px; height:86px;"></a></td>
              </tr>
          </table>
            <!-- Email Header : END -->
            
            <!-- Hero Image, Flush : BEGIN -->
            <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="600" class="email-container">
           <!-- <tr>
                <td class="full-width-image"><img src="https://res.cloudinary.com/ordering/image/upload/v1498242360/background_rehs3w.png" width="600" alt="alt_text" border="0" align="center" style="width: 100%; max-width: 600px; height: auto;"></td>
              </tr>-->
         
            <tr>
                <td dir="ltr" align="center" valign="top" width="100%" style="padding: 50px 10px;"><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td width="25%" class="stack-column-center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>';
                            if($ost==5 || $ost==2){
								$msg .=' <td dir="ltr" valign="top" style="padding: 20px 10px 20px;"><img src="https://res.cloudinary.com/ordering/image/upload/v1498242411/cancel-icon_zeabrn.png"   alt="alt_text" border="0" class="center-on-narrow"></td>';
							}
							if($ost==4 ){
								$msg .=' <td dir="ltr" valign="top" style="padding: 20px 10px 20px;"><img src="https://res.cloudinary.com/ordering/image/upload/v1498242437/notification-icon-2_zsqn5u.png"   alt="alt_text" border="0" class="center-on-narrow"></td>';
							}
							if($ost==1 ){
								$msg .=' <td dir="ltr" valign="top" style="padding: 20px 10px 20px;"><img src="https://res.cloudinary.com/ordering/image/upload/v1498242461/complete_xrwmtc.png"   alt="alt_text" border="0" class="center-on-narrow"></td>';
							}
                          $msg .='</tr>
                          </table></td>
                          <td width="75%" class="stack-column-center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                            <td dir="ltr" valign="top" style="padding:  20px 0px; vertical-align:text-top">
                            <h2 style="color:#ef1962; font-size:18px;">'.$lang_resource['EMAIL_CURRENT_ORDER_STATUS_FOR_HI'].' '.$user_name.'!</h2>
                            <p style="color:#555; font-size:16px; ">'.$lang_resource['EMAIL_CURRENT_ORDER_STATUS_FOR_ORDER_NO'].' '.$order->id.' '.$lang_resource['EMAIL_TEMPLATE_IS'].':</p>
<p style="color:#ef1962; font-size:16px; ">'.$order->status.'</p>';
if($roworderdata['count_down']>0){
$msg .='<p style="color:#555; font-size:16px; ">'.$lang_resource['THE_EXPECTED_WAIT_TIME_IS'].': '.$roworderdata['count_down'].' '.$lang_resource['MINUTES'].'</p>';
}
if($order->driver_comment){
	$msg .='<p style="color:#555; font-size:16px; "><img src="https://res.cloudinary.com/ordering/image/upload/v1498242495/driver_lpmrur.png" style="vertical-align: middle; margin-right:10px;">'.$lang_resource['BUSINESS_EMAIL_DRIVERS_COMMENTS'].': <span style="color:#ef1962;">'.$order->driver_comment.'</span></p>';
}
if($order->comment){
	$msg .='<p style="color:#555; font-size:16px; "><img src="https://res.cloudinary.com/ordering/image/upload/v1498242525/res-icon_vksx9n.png" style="vertical-align: middle; margin-right:10px;">'.$lang_resource['ORDER_DETAILS_DISHES_COMMENTS'].':<span style="color:#ef1962;">'.$order->comment.'</span></p>';
}
                            
                           $msg .=' </td>
                          </tr>
                          </table></td>
                        
                    </tr>
                  </table></td>
              </tr>
            <!-- Thumbnail Left, Text Right : END --> 
           
            
          </table>
            <!-- Email Footer : BEGIN -->
            <table align="center" width="600" class="email-container" style="background:#1d1d1d; ">
            <tr>
                <td style="padding: 40px 18px;width: 100%;font-size: 15px; font-family: Open Sans, sans-serif; mso-height-rule: exactly; line-height:24px; text-align: center; color: #fff;">'.$lang_resource['FORGOT_PASSWORD_FOOTER_TEXT1']. '<a href="" style="color:#fff;">'." ".''.$lang_resource['CONTACT_US_EMAIL'].'</a> '.$lang_resource['EMAIL_FOOTER_CALL'].''." ".''.$lang_resource['CONTACT_US_NO'].'<br>© '.$curyear.'<a href="" style="color:#fff;"> '.$txtsitename.'</a></td>
              </tr>
          </table>
            <!-- Email Footer : END -->
            
          </center></td>
      </tr>
    </table>
</body>
</html>';
?>