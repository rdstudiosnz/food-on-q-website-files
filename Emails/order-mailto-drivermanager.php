<?php
error_reporting(E_ALL);
//session_start();
pg_query($link, "DEALLOCATE ALL");

		
pg_prepare($link,'sql5b','SELECT * from w_business WHERE id=$1');
$result23b = pg_execute($link,'sql5b',array($bus_id));
$row23b = pg_fetch_array($result23b);
$sitecurrency = currency_symbol($row23b['currency']);
$deliverytime = $row23b['deliverytime'];
$pickuptime = $row23b['pickuptime'];
$bstreet = $row23b['street'];
$bcolony = $row23b['colony'];
$btel = $row23b['tel'];
$bname=	$row23b['name'];				 


//$serverlink = 'http://'.$_SERVER['HTTP_HOST'];
$serverlink = $sitename;

$msgDriverManger =<<<END
<html>
    <head>
    <meta charset="utf-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Use the latest (edge) version of IE rendering engine -->
    <title>EmailTemplate-Responsive</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"> 
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
    <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->

    <!-- CSS Reset -->
    <style type="text/css">
/* What it does: Remove spaces around the email design added by some email clients. */
      /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
html,  body {
	margin: 0 !important;
	padding: 0 !important;
	height: 100% !important;
	width: 100% !important;
	font-family: 'Open Sans', sans-serif !important;
}
/* What it does: Stops email clients resizing small text. */
* {
	-ms-text-size-adjust: 100%;
	-webkit-text-size-adjust: 100%;
}
/* What it does: Forces Outlook.com to display emails full width. */
.ExternalClass {
	width: 100%;
}
/* What is does: Centers email on Android 4.4 */
div[style*="margin: 16px 0"] {
	margin: 0 !important;
}
/* What it does: Stops Outlook from adding extra spacing to tables. */
table,  td {
	mso-table-lspace: 0pt !important;
	mso-table-rspace: 0pt !important;
}
/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
table {
	border-spacing: 0 !important;
	border-collapse: collapse !important;
	table-layout: auto !important;
	margin: 0 auto !important;
}
table table table {
	table-layout: auto;
}
/* What it does: Uses a better rendering method when resizing images in IE. */
img {
	-ms-interpolation-mode: bicubic;
}
/* What it does: Overrides styles added when Yahoo s auto-senses a link. */
.yshortcuts a {
	border-bottom: none !important;
}
/* What it does: Another work-around for iOS meddling in triggered links. */
a[x-apple-data-detectors] {
	color: inherit !important;
}
</style>

    <!-- Progressive Enhancements -->
    <style type="text/css">
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #d7094f !important;
            border-color: #d7094f !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid-centered {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* And center justify these ones. */
            .fluid-centered {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
        
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
                
        }

    </style>
    </head>
	<body bgcolor="#fff" width="100%" style="margin: 0;" yahoo="yahoo">
    <table bgcolor="#fff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse; ">
      <tr>
        <td><center style="width: 100%;">
            <!-- Email Header : BEGIN -->
            <table align="center" width="600" class="email-container" style="border-bottom:1px solid #dedede;">
            <tr>
                <td style="padding: 20px 0; text-align: center"><a href=""><img src="{$logo->secure_url}"  border="0" style="width:317px; height:70px;"></a></td>
              </tr>
          </table>
          <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="600" class="email-container">
            <!-- 1 Column Text : BEGIN -->
            <tr>
                <td style="padding: 30px 20px; text-align: center; font-family: Open Sans, sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 30px; color: #555555;">
                	
                    <p style="font-size:22px; color:#ef1962;">{$lang_resource['EMAIL_TEMPLATE_DETAILS_OF_CUSTOMER_ORDER']} : $order->id</p>
                    <p style="font-size:22px; color:#ef1962;">{$lang_resource['EMAIL_TEMPLATE_THIS_ORDER_ASSIGNED_TO_DRIVER']} : $driver_name</p>
                    
                    
                </td>
                <!-- Button : Begin -->
              </tr>
            <!-- 1 Column Text : BEGIN --> 
          </table>
            <!-- Email Header : END --> 
            <!-- Email Body : BEGIN -->
            <table cellspacing="0" cellpadding="0"  align="center" bgcolor="#ffffff" width="600" class="email-container" style="border:1px solid #d1d1d1; margin-top:0px !important">
          
            <tr>
                <td align="center" valign="top" style="padding: 10px;"><table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                    <td width="50%" class="stack-column-center"><table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                        <td style="font-family: 'Open Sans', sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 20px 10px 10px; text-align: left;" class="center-on-narrow">
END;
if($data->buyer->deliveryType == "delivery"){
	if($deliverytime == "" || $deliverytime == "undefined"){
		$d_time = '00:00';							
	}else{
		$d_time = $deliverytime;				
	}
	$est_time = $lang_resource['ESTIMATE_DELIVERY_TIME'];
}else{
	if($pickuptime == "" || $pickuptime == "undefined"){
		$d_time = '00:00';							
	}else{
		$d_time = $pickuptime;				
	}
	$est_time = $lang_resource['ESTIMATE_PICKUP_TIME'];	
}
$msgDriverManger .=<<<END
		<h3 style="margin:0px; color:#3a3a3a; font-size:18px;margin-bottom:7px;">{$lang_resource['FROM_HEADING']}</h3>
                        <p style="margin:0px; color:#555; font-size:14px;margin-bottom:7px;">{$data->business[0]->name}</p>
                        <p style="margin:0px; color:#555; font-size:14px;margin-bottom:7px;">$bstreet</p>
                        <p style="margin:0px; color:#555; font-size:14px;margin-bottom:7px;">+$btel</p>
                        </td>
                      </tr>
                      </table></td>
                    <td width="50%" class="stack-column-center"><table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                        <td style="font-family: 'Open Sans', sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 20px 10px 10px; text-align: right;" class="center-on-narrow"> <h3 style="margin:0px; color:#3a3a3a; font-size:18px;margin-bottom:7px;">{$lang_resource['TO_HEADING']}</h3>
                        <p style="margin:0px; color:#555; font-size:14px;margin-bottom:7px;">{$data->buyer->name}</p>
                        <p style="margin:0px; color:#555; font-size:14px;margin-bottom:7px;">{$data->buyer->address}</p>
                        <p style="margin:0px; color:#555; font-size:14px;margin-bottom:7px;">+{$data->buyer->tel}</p> </td>
                      </tr>
                      </table></td>
                  </tr>
                  </table></td>
              </tr>		  
END;

$twilio_phone;
$twilio_enabled;
$twilio_order = "";
$business = $data->business[0];
$twilio_phone = $business->twiliophone;
//$twilio_enabled = $business->twilioenabled;

pg_prepare($link,'sql4','SELECT buys from w_business WHERE id=$1');
$result2 = pg_execute($link,'sql4',array($business->id));
if (pg_num_rows($result2)==1)  
	while($row2 = pg_fetch_array($result2)){
		pg_prepare($link,'sqls','UPDATE w_business SET buys=$2 WHERE id=$1');
		pg_execute($link,'sqls',array($business->id,intval($row2['buys'])+1));
	}

$total = 0;
foreach ($business->dishes as $dish){
	if($dish->options) {
		$productOptionHtml =  Margeslash55($dish->options);  
	} else {
		$productOptionHtml ='';
	}
	$dishcomments = str_replace("%20", " ", ucfirst($dish->comments));
$msgDriverManger .=<<<END
<tr>
     <td dir="ltr" align="center" valign="top" width="100%" style="padding: 10px;"><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"  style="table-layout:auto;" >
<tr>
                    <td   style="vertical-align:top;"><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:auto !important;" >
                        <tr>
                          <td width="15% !important;" dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; vertical-align:top;color: #3c3c3c; padding: 10px; text-align: left;"  >$dish->quantity x</td>
                          <td width="55% !important; dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; vertical-align:top; color: #3c3c3c; padding: 10px; text-align: left;" >$dish->name $productOptionHtml<p style="color:#939393; font-size:14px; margin:0px 0px 5px 0px;"">$dishcomments</p>
                        
                        </td>
                        <td width="30% !important; dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 14px; mso-height-rule: exactly; vertical-align:top;line-height: 20px; color: #3c3c3c; padding: 10px; text-align: right;" >$sitecurrency $dish->total</td>
                      </tr>
                      </table></td>
                  </tr>
				  </table>
				  </td>
				  </tr>
END;
$total = $total + $dish->total;
$subtotal = $total;
}
$msgDriverManger .='</table>
            <!-- Thumbnail Right, Text Left : END -->
          
<table cellspacing="0" cellpadding="0"  align="center" bgcolor="#f5f5f5" width="600" class="email-container" style="border:1px solid #d1d1d1;border-top:none; " >
            <tr>
                <td dir="ltr" align="center" valign="top" width="100%" style="padding: 10px;"><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"  style="table-layout:auto;" >
                    <tr>
                    <td   style="vertical-align:top;"><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:auto !important;" >';

if($data->buyer->taxtype == 1){
	$total = $total + $business->shipping + $data->tax;
	$taxstring = $lang_resource['FRONT_MAIN_EMAIL_TAX_NOT_INCLUDED'];
}else{
	$total = $total + $business->shipping;
	$taxstring = $lang_resource['FRONT_MAIN_EMAIL_TAX_INCLUDED'];
}
$taxpercentage = GetDecimalPoint($data->buyer->tax);

$tipsprice = GetDecimalPoint($data->buyer->tips);

if ($data->buyer->tips > 0){
	$total = $total	+ $data->buyer->tips;
}
$total = GetDecimalPoint($total);
$deltype = $data->buyer->deliveryType;

if ($business->shipping=='0.00')
	$shippingcaption = $lang_resource['FRONT_MAIN_HOME_DELIVERY'];
else
	$shippingcaption = $lang_resource['FRONT_MAIN_HOME_DELIVERY_COST'];
	//$ordercmts = str_replace("%20", " ", ucfirst($order->buyer->comments));

$msgDriverManger .='<tr><td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; vertical-align:top; color: #494949; padding: 10px; text-align: left;" >'.$lang_resource['SUBTOTAL'].'</td>
                        <td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; vertical-align:top; color: #494949; padding: 10px; text-align: right;" >'.$sitecurrency.' '.$subtotal.'</td>
                      </tr>';

$msgDriverManger .='<tr><td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; vertical-align:top; color: #494949; padding: 10px; text-align: left;" >'.$shippingcaption.'</td>
                        <td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 14px; mso-height-rule: exactly; vertical-align:top;line-height: 20px; color: #494949; padding: 10px; text-align: right;" >'.$sitecurrency.' '.convert2Digit($business->shipping).'</td>
                      </tr>';

if($data->discountprice !=0 && isset($data->discountcategory)){
	if($data->discounttype == 1)
		$discountcaption = $lang_resource['SHOPPING_DISCOUNT_TEXT'] ." (" .$data->discountrate ."%)";
	else if($data->discounttype == 2)
		$discountcaption = $lang_resource['SHOPPING_DISCOUNT_TEXT'] ;

	$total = $total - $data->discountprice;
	$total = GetDecimalPoint($total);

	//$discmmnts = ucfirst(strtolower($data->discountcomments));
$msgDriverManger .='<tr>
		<td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; vertical-align:top; color: #494949; padding: 10px; text-align: left;" >'.$discountcaption.'</td>                                 
		<td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size:14px; mso-height-rule: exactly; line-height: 20px; vertical-align:top; color: #494949; padding: 10px; text-align: right;" >'.$sitecurrency.' '.$data->discountprice.'</td>
		</tr>';
}
if($data->tax > 0){
$msgDriverManger .='<tr>
	<td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; vertical-align:top; color: #494949; padding: 10px; text-align: left;" >'.$lang_resource['Tax_V2'].'</td>                                 
		<td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size:14px; mso-height-rule: exactly; line-height: 20px; vertical-align:top; color: #494949; padding: 10px; text-align: right;" >'.$sitecurrency.' '.$data->tax.'</td>
		</tr>';
}
if($data->buyer->tips > 0){
$txstr = ucfirst(strtolower($taxstring));
$msgDriverManger .='<tr>
	<td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; vertical-align:top; color: #494949; padding: 10px; text-align: left;" >'.$lang_resource['DELIVERY_TIP_DRIVER'].'('.$taxpercentage .'%)'.$txstr.'</td>                                 
		<td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size:14px; mso-height-rule: exactly; line-height: 20px; vertical-align:top; color: #494949; padding: 10px; text-align: right;" >'.$sitecurrency.''. $tipsprice.'</td>
		</tr>';
}
if ($business->paymethod->cash==true)
	$paymethod = $lang_resource['FRONT_MAIN_EMAIL_UPON_RECEIVING_PAY_CASH'];
if ($business->paymethod->card==true)
	if ($paymethod=='')
		$paymethod = $lang_resource['FRONT_MAIN_EMAIL_UPON_RECEIVING_PAY_CASH'];
	else
		$paymethod .= $lang_resource['AND_CARD'];

if ($data->business[0]->paymethod->transactium==true) {
				if ($paymethod=='')
				   if ($paymentid=='Failure'){
					$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_NOT_TRANSACTIUM'];
					}else{
					$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_TRANSACTIUM']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";	
					}
					
					
			}
			
if ($data->business[0]->paymethod->pexpress==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYMENTEXPRESS']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";		
}

if ($data->business[0]->paymethod->maksekeskus==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_MAKSEKESKUS']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";		
}

if ($data->business[0]->paymethod->voguepay==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_VOGUEPAY']." "."(" . $paymentid . ")";	
}

if ($data->business[0]->paymethod->skrill==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_SKRILL']." "."(" . $paymentid . ")";	
}

if ($data->business[0]->paymethod->payeezy==true){
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYEEZY']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->payu==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYU']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->stripe==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_STRIPE']." "."(" . $paymentid . ")";	
}
/*if ($data->business[0]->paymethod->btrans==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_BTRANS']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->bsa==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_BSA']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->azul==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['PAYMENT_GATEWAY_ALL_AZUL_PAYMENT_SUCCESS']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->quickpay==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_QUICKPAY']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->paynl==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYNL']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->zaakpay==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_ZAAKPAY']." "."(" . $paymentid . ")";	
}
if ($data->business[0]->paymethod->cash==true) {
	if ($paymethod=='')							
		$paymethod = $lang_resource['FRONT_MAIN_EMAIL_UPON_RECEIVING_PAY_CASH'];
}*/
$msgDriverManger .='
<tr>
                          <td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 22px; mso-height-rule: exactly; line-height: 20px; vertical-align:top; color: #494949; padding: 10px; text-align: left;" >'.$lang_resource['DELIVERY_TOTAL'].'</td>
                        <td dir="ltr" valign="top" style="font-family: Open Sans, sans-serif; font-size: 22px; mso-height-rule: exactly; vertical-align:top;line-height: 20px; color: #ef1962; padding: 10px; text-align: right;" >'.$sitecurrency.''. $data->total.'</td>
                      </tr>
                      </table></td>
                  </tr>
                    
                    
                  </table></td>
              </tr>
          </table>
            <!-- Email Body : END --> 
            <table align="center" width="600" class="email-container" style="background:#fff;"> 
            <tr>
                <td style="padding: 20px 18px;width: 100%;font-size: 15px; font-family: Open Sans, sans-serif; mso-height-rule: exactly; line-height:24px; text-align: center; color: #fff;"></td>
              </tr>
          </table>
            <!-- Email Footer : BEGIN -->
            <table align="center" width="600" class="email-container" style="background:#1d1d1d;" margin-top:50px !important;> 
            <tr>
                <td style="padding: 40px 18px;width: 100%;font-size: 15px; font-family: Open Sans, sans-serif; mso-height-rule: exactly; line-height:24px; text-align: center; color: #fff;">'.$lang_resource['DRIVER_EMAIL_TEMPLATE_FOOTER_TEXT1'].''." ".''.$bname.' at +'.$btel.', '.$lang_resource['DRIVER_EMAIL_TEMPLATE_FOOTER_TEXT2'].''." ".' <a href="" style="color:#fff;"> '.$lang_resource['CONTACT_US_EMAIL'].'</a> '.$lang_resource['DRIVER_EMAIL_TEMPLATE_FOOTER_TEXT3'].''." ".' '.$lang_resource['CONTACT_US_NO'].'.<br>
 © '.$curyear.' <a href="" style="color:#fff;">'.$txtsitename.'</a></td>
              </tr>
          </table>
            <!-- Email Footer : END -->
            
          </center></td>
      </tr>
    </table>
</body>
</html>
';
		
function Margeslash55($text){
	
	$res = explode("_@_",$text);
	
	$options_record = Array();
	foreach($res as $po){
		$poption = explode("@u@",$po);
		$a=new stdClass();
		$a->optionheader=$poption[0];
		$a->optionchoice=$poption[1];
		if($poption[0]!=""){	
			array_push($options_record,$a);
		}
	}
	$lasti =0;
	$html ='';
	foreach($options_record as $each_record){
	if($lasti == 0 ) {
	$html .='<p style="color:#939393; font-size:14px; margin:0px 0px 5px 0px;">'.$each_record->optionheader.'</p>';
	$html .= '<p style="color:#939393; font-size:14px; margin:0px 0px 5px 0px;"">'.$each_record->optionchoice.'</p>';
	
	}
	else if($previousHeader == $each_record->optionheader ) {
		$html .= '<p style="color:#939393; font-size:14px; margin:0px 0px 5px 0px;"">'.$each_record->optionchoice.'</p>';
	}
	else if($previousHeader != $each_record->optionheader) {
		$html .='<p style="color:#939393; font-size:14px; margin:0px 0px 5px 0px;">'.$each_record->optionheader.'</p>';
		$html .= '<p style="color:#939393; font-size:14px; margin:0px 0px 5px 0px;"">'.$each_record->optionchoice.'</p>';
		
		}
	$previousHeader = $each_record->optionheader;
	$lasti++;
	}
	return $html;
}		
/*function convert2Digit($num){
	if($num<10){
		return $num;
	}else{
		return $num;
	}
}	*/	

?>
