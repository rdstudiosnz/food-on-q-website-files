<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
} 
// Access-Control headers are received during OPTIONS requests
/*if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}*/
$uri =  $_SERVER["REQUEST_URI"]; //it will print full url
$uriArraylist = explode('/', $uri); //convert string into array with explode


$usersfolder = $uriArraylist[3];

/*if(!file_exists('../../../user/'.$usersfolder.'/')){
	
	header("location: http://".$_SERVER['HTTP_HOST']."/404.html");
	exit;
	}*/

require_once('../../common/settings.php');
$getData = parse_url($_SERVER['REQUEST_URI']);
$langurl = explode("&",$getData['query']);
$datakey = array();
foreach($langurl as $val) {
	$data = explode("=",$val);
	$datakey[$data[0]] = $data[1];
}
$lang='1';
$a = $datakey['a'];
$o = $datakey['o'];
$ak = $datakey['ak'];
$m = $datakey['m'];
$dt = $datakey['dt'];
$u = $datakey['u'];
$p = $datakey['p'];

$link = ConnectDB('',$usersfolder);

require_once('../../common/multilanguage.php');
require_once('../../common/ordersapp.php');

pg_prepare($link,'sqldefltlng','SELECT * FROM w_lang_setting WHERE opdefault=$1');
$dlngresult = pg_execute($link,'sqldefltlng',array('1'));
$drow = pg_fetch_array($dlngresult);
$lang = $drow['id'];

global $lang_resource;
$lang_resource = GetLangFileStaticIOSLANG($lang,$link); 

if(isset($o) && $o!="" && isset($ak) && $ak!=""){

	$link = ConnectDB('',$usersfolder);
	$order=$o;

	//fetch bus id for order
	$sqlbus = "SELECT data FROM w_orders WHERE id = $1";
	pg_prepare($link,'sqlbus',$sqlbus);
	$bus_fetch_sql = pg_execute($link,'sqlbus',array($order));

	while($bus_array = pg_fetch_array($bus_fetch_sql)){
		$bus_data = json_decode($bus_array[0]);
	}

	$rest = $bus_data->business[0]->id;
	//time zone time
	pg_prepare($link,'sqlbid',"SELECT * FROM w_orders WHERE id=$1");
	$result12 = pg_execute($link,'sqlbid',array($order));
	if(pg_num_rows($result12)>0){
		while($row12 = pg_fetch_array($result12)){
			$data= json_decode($row12['data']);
			$bid=$data->business[0]->id;
		}
		pg_prepare($link,'sqltimzone',"SELECT timezone, provider FROM w_business WHERE id=$1");
		$result13 = pg_execute($link,'sqltimzone',array($bid));
		while($row13 = pg_fetch_array($result13)){
			$timezone=$row13['timezone'];
			$provider=$row13['provider'];
		}
		$date = new DateTime('now', new DateTimeZone($timezone));
		$localtime = $date->format('h:i:s a');
		$update_date=$date->format('Y-m-d');	
	} else {
		pg_prepare($link,'sqlcontimzone',"SELECT value FROM w_configs WHERE name=$1");
		$result14 = pg_execute($link,'sqlcontimzone',array('defaulttimezone'));
		while($row14 = pg_fetch_array($result14)){
				$timezone1=$row14['value'];
		}
		//echo $timezone1;die;
		$date = new DateTime('now', new DateTimeZone($timezone1));
		$localtime = $date->format('h:i:s a');
		$update_date=$date->format('Y-m-d');
	}
	/*$time = explode("_",$m);
	if($time['0'] == ''){
		$countDown = 0;
	}
	else{
		$countDown = $time['0'];
	} */
	if(is_null($m) || $m ==''){
		$countDown = 0;
	}
	else{
		$time = explode("_",$m);
		$countDown = $time['0'];
	}
	$endTime = strtotime("+".$countDown." minutes", strtotime($localtime));
	$endTime1 = date('h:i:s a', $endTime);
	
	$nowdate = $update_date.' '.$localtime;
	$newendTime = strtotime("+".$countDown." minutes", strtotime($nowdate));
	$endTimedate = date('Y-m-d h:i:s a', $newendTime);
	//time zone time

	$sql="UPDATE w_orders SET status=$1,comment=$2,printer_stime=$3,set_time=$5,deliverytime=$6,printer_comment=$7 WHERE id=$4";
	$status=0;
	$desc="";
	$printer_stime="00:00";

	if($ak=="Rejected"){
		$status=5;
	}else if($ak=="Accepted"){
		$status=7;
	}

	if(isset($dt)){
		if($status==7){
			$desc = $lang_resource['EMAIL_PRINTER_STATUS_UPDATE_RESTAURANT'].' at :'.$endTime1;
		}else if($status==5){
			$desc=$m;
		}
		$printer_stime=$dt;
	}

	//get printer file path
	//Fetch Dynamic printer path for order business
	
	$dirPath = "/var/www/html/api/sys/user/".$usersfolder."/orders";
	$DynamicPrinterPath = "/var/www/html/api/sys/user/".$usersfolder."/orders/".$rest.".txt"; //default path
	$PathQueue = $rest;

	//1. check main settings
	pg_prepare($link,'sql_print',"SELECT * FROM w_printerpath");
	$result_print = pg_execute($link,'sql_print',array());

	while($array_print = pg_fetch_array($result_print)){
		$array_print_val = json_decode($array_print['printer_restaurant']);
		if(($array_print_val[0] == -1) || in_array($rest,$array_print_val)){ 
			//check if assigned for all or particular business
			$DynamicPrinterPath = "/var/www/html/api/sys/user/".$usersfolder."/orders/".$array_print['path'].".txt";
			$PathQueue = $array_print['path'];
			break;
		}
	}

	pg_prepare($link,'sql3',$sql);
	pg_execute($link,'sql3',array($status,$desc,$printer_stime,$order,$endTime1,$endTimedate,$m));
		//user push accept
		$credential =Pushcredential('',$usersfolder);
		$UserappType = $credential->orderingappType;
		pg_prepare($link,'sqldrvrdata','SELECT * FROM w_orders WHERE id=$1');
		$result1234 = pg_execute($link,'sqldrvrdata',array($order));
		$row123=pg_fetch_array($result1234);
		$usrId = $row123['usr'];
		pg_prepare($link,'sqlgcmuser','SELECT * FROM w_gcm WHERE login_status=1 and user_id=$1 and app_type=$2');
		$userresultgcm = pg_execute($link,'sqlgcmuser',array($usrId,$UserappType));
		$User_registatoin_ids = array();
		$User_ios_ids = array();
		while($userrowgcm=pg_fetch_array($userresultgcm)){
			array_push($User_registatoin_ids, $userrowgcm['gcm_id']);
			array_push($User_ios_ids, $userrowgcm['apns_id']);		
		}
		if($status == 7){
			 //$orderTitle = $lang_resource['PUSH_ORDER'].$order." ".$lang_resource['PUSH_ORDER_ACCEPTED_BY_BUSINESS_TIME']." at ".$dt;
			 $orderTitle = $lang_resource['EMAIL_PRINTER_STATUS_UPDATE_RESTAURANT'] .'#'.$order.' at :'.$endTime1;
			$status1= $lang_resource['EMAIL_PRINTER_STATUS_UPDATE_RESTAURANT'];
		}
		else if($status == 5){
			 $orderTitle = $lang_resource['EMAIL_PRINTER_STATUS_UPDATE_RESTAURANT_REJECT'] .'#'.$order;
			$status1 = $lang_resource['EMAIL_PRINTER_STATUS_UPDATE_RESTAURANT_REJECT'];
		}
		$response['push'] = sendUserMessage($order,$orderTitle,$status1, $User_registatoin_ids,$User_ios_ids,1,$credential->ordering_app_id,$credential->ordering_app_token);
		
		//push
	
	if(file_exists($DynamicPrinterPath)){

		/*$files = array();
		  if ($dh = opendir($dirPath)){
			while (($file = readdir($dh)) !== false){
				array_push($files,$file);
			}			
		  }*/
		

		//$files =array_diff(scandir($dirPath), array('..', '.')); //glob($dirPath.'/*.txt');
		//$files =array_map('basename', glob($dirPath . "/*.{txt}", GLOB_BRACE));
		$files =glob($dirPath.'/*.txt');
		$printer_array = array();

		foreach($files as $file){
			$valx1 = explode(".",$file);
			//echo $valx1[0];
			//$valx2 = array_shift( explode('_', $valx1[0]) );			
			$temp1   = explode('_',$valx1[0]);
			$temp2   = array_slice($temp1, 0, 3);
			//$valx2 = explode('_', $temp2);	
			//$valx2[0]=$valx2;	
			$valx2 = $temp1[0];
//echo $valx2.'$$$$$';
//echo "/var/www/html/api/sys/user/".$usersfolder."/orders/".$PathQueue.'#####';
			if($valx2 == "/var/www/html/api/sys/user/".$usersfolder."/orders/".$PathQueue){
				array_push($printer_array,$file);
			}
		}

		$file_size = count($printer_array);

		if($file_size > 1){
			unlink($DynamicPrinterPath);
			$fp = fopen($DynamicPrinterPath, "w");

			copy($printer_array[1], $DynamicPrinterPath);
			unlink($printer_array[1]);
			fclose($fp);
		}else if($file_size == 1){
			unlink($DynamicPrinterPath);
			$fp = fopen($DynamicPrinterPath, "w");
			fclose($fp);
		}
	}
	
	include_once('../../common/phpmailer/PHPMailerAutoload.php');

		//ORDER DETAILS
		$order_id = $order;
		if($ak == "Rejected"){
			$order_status = $lang_resource['ORDER_REJECTED_RESTAURANT'];//"Rejected";
		}else if($ak == "Accepted"){
			$order_status = $lang_resource['ORDER_STATUS_ACCEPTEDBYRESTAURANT'];//"Accepted";
		}
		$order_comment = $desc;

	$curyear=  date('Y');

	pg_prepare($link,'sqlconfig2ef','SELECT value FROM w_configs WHERE name=$1');
	$resultef = pg_execute($link,'sqlconfig2ef',array('sent_from_email'));
	$rowef = pg_fetch_array($resultef);

	pg_prepare($link,'sqlconfig2sn','SELECT value FROM w_configs WHERE name=$1');
	$resultsn = pg_execute($link,'sqlconfig2sn',array('sitename'));
	$rowsn = pg_fetch_array($resultsn);

	pg_prepare($link,'sqlconfig2','SELECT value FROM w_configs WHERE name=$1');
	$result5 = pg_execute($link,'sqlconfig2',array('logo_admin_header'));
	//$rowcon = pg_fetch_array($result5);
	while($row5 = pg_fetch_array($result5)){
		$logo=json_decode($row5['value']);
	}

	$serverlink = 'http://'.$_SERVER['HTTP_HOST'];
	
	$sql="SELECT data FROM w_orders WHERE id = $1";
	pg_prepare($link,'sql7',$sql);
	$res =  pg_execute($link,'sql7',array($order_id));
	$data = pg_fetch_array($res);
	$datas =   json_decode($data['data']);
	$user_mail = $datas->buyer->email;
	
	$msg = '<html>
	<head>
	    <meta charset="utf-8">
	    <!-- utf-8 works for most cases -->
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- Forcing initial-scale shouldnt be necessary -->
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <!-- Use the latest (edge) version of IE rendering engine -->
	    <title>EmailTemplate-Responsive</title>
	    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"> 
	    <!-- The title tag shows in email notifications, like Android 4.4. -->
	    <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
	    <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->

	    <!-- CSS Reset -->
	    <style type="text/css">
	/* What it does: Remove spaces around the email design added by some email clients. */
	      /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
	html,  body {
		margin: 0 !important;
		padding: 0 !important;
		height: 100% !important;
		width: 100% !important;
		font-family: Open Sans, sans-serif !important;
	}
	/* What it does: Stops email clients resizing small text. */
	* {
		-ms-text-size-adjust: 100%;
		-webkit-text-size-adjust: 100%;
	}
	/* What it does: Forces Outlook.com to display emails full width. */
	.ExternalClass {
		width: 100%;
	}
	/* What is does: Centers email on Android 4.4 */
	div[style*="margin: 16px 0"] {
		margin: 0 !important;
	}
	/* What it does: Stops Outlook from adding extra spacing to tables. */
	table,  td {
		mso-table-lspace: 0pt !important;
		mso-table-rspace: 0pt !important;
	}
	/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
	table {
		border-spacing: 0 !important;
		border-collapse: collapse !important;
		table-layout: auto !important;
		margin: 0 auto !important;
	}
	table table table {
		table-layout: auto;
	}
	/* What it does: Uses a better rendering method when resizing images in IE. */
	img {
		-ms-interpolation-mode: bicubic;
	}
	/* What it does: Overrides styles added when Yahoos auto-senses a link. */
	.yshortcuts a {
		border-bottom: none !important;
	}
	/* What it does: Another work-around for iOS meddling in triggered links. */
	a[x-apple-data-detectors] {
		color: inherit !important;
	}
	</style>

	    <!-- Progressive Enhancements -->
	    <style type="text/css">
	        
	        /* What it does: Hover styles for buttons */
	        .button-td,
	        .button-a {
	            transition: all 100ms ease-in;
	        }
	        .button-td:hover,
	        .button-a:hover {
	            background: #d7094f !important;
	            border-color: #d7094f !important;
	        }

	        /* Media Queries */
	        @media screen and (max-width: 600px) {

	            .email-container {
	                width: 100% !important;
	            }

	            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
	            .fluid,
	            .fluid-centered {
	                max-width: 100% !important;
	                height: auto !important;
	                margin-left: auto !important;
	                margin-right: auto !important;
	            }
	            /* And center justify these ones. */
	            .fluid-centered {
	                margin-left: auto !important;
	                margin-right: auto !important;
	            }

	            /* What it does: Forces table cells into full-width rows. */
	            .stack-column,
	            .stack-column-center {
	                display: block !important;
	                width: 100% !important;
	                max-width: 100% !important;
	                direction: ltr !important;
	            }
	            /* And center justify these ones. */
	            .stack-column-center {
	                text-align: center !important;
	            }
	        
	            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
	            .center-on-narrow {
	                text-align: center !important;
	                display: block !important;
	                margin-left: auto !important;
	                margin-right: auto !important;
	                float: none !important;
	            }
	            table.center-on-narrow {
	                display: inline-block !important;
	            }
	                
	        }

	    </style>
	    </head>
		<body bgcolor="#fff" width="100%" style="margin: 0;" yahoo="yahoo">
	    <table bgcolor="#fff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse; ">
	      <tr>
	        <td><center style="width: 100%;">
	           
	            <table align="center" width="600" class="email-container" style="border:1px solid #dedede;">
	            <tr>
	                <td style="padding: 20px 0; text-align: center;"><a href=""><img src="'.$logo->secure_url.'"  border="0"  style="width:317px; height:86px;"></a></td>
	              </tr>
	          </table>
			  <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="600" class="email-container">
	         
	            <tr>
	                <td dir="ltr" align="center" valign="top" width="100%" style="padding: 50px 10px;"><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
	                    <tr>
	                        
	                          <td width="100%" class="stack-column-center"><table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
	                            <tr>
	                            <td dir="ltr" valign="top" style="padding:  20px 0px; vertical-align:text-top">
	                            <h2 style="color:#ef1962; font-size:18px; text-align:center;">'.$lang_resource['ORDER_EMAIL_TEMPLATE_HI'].''." ".''.$datas->buyer->name.'</h2>';
	                           $msg.='<p style="color:#555; font-size:16px;text-align:center; ">'.$lang_resource['PRINTER_EMAIL_THE_STATUS_CHANGE'].'</p>';
	                           if($ak == "Accepted"){
							   		$msg .='<p style="color:#555; font-size:16px; text-align:center;">'.$lang_resource['PRINTER_ORDER_ACCEPTED'].''." ".''.$order_id.''." ".''.$lang_resource['PRINTER_ORDER_AT'].''." ".''.$dt.' </p>
							   <p style="color:#555; font-size:16px; text-align:center;">'.$lang_resource['THE_EXPECTED_WAIT_TIME_IS'].' '.$countDown.' '.$lang_resource['PRINTER_ORDER_MIN'].'</p>';
							   }
							   else{
							   		$msg .='<p style="color:#555; font-size:16px; text-align:center;">'.$lang_resource['PRINTER_ORDER_REJECTED'].''." ".''.$order_id.'</p>';
							   }
	                           
	                           $msg .='</td>
	                          </tr>
	                          </table></td>
	                        
	                    </tr>
	                  </table></td>
	              </tr>
				  </table>
	            <!-- Email Footer : BEGIN -->
	            <table align="center" width="600" class="email-container" style="background:#1d1d1d; ">
	            <tr>
	                <td style="padding: 40px 18px;width: 100%;font-size: 15px; font-family: Open Sans, sans-serif; mso-height-rule: exactly; line-height:24px; text-align: center; color: #fff;">'.$lang_resource['FORGOT_PASSWORD_FOOTER_TEXT1']. '<a href="" style="color:#fff;">'." ".''.$lang_resource['CONTACT_US_EMAIL'].'</a> '.$lang_resource['EMAIL_FOOTER_CALL'].''." ".''.$lang_resource['CONTACT_US_NO'].'.<br>© '.$curyear.'<a href="" style="color:#fff;"> '.$txtsitename.'</a></td>
	              </tr>
	          </table>
	            <!-- Email Footer : END -->
	            
	          </center></td>
	      </tr>
	    </table>
	</body>
	</html>';
		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
		$mail->PluginDir = "";
		$row = FetchAllsettingsCustomMailchmp1($link);
		$mail->isSMTP();
		$mail->Host = 'smtp.mandrillapp.com';
		$mail->SMTPAuth = true;
		$mail->Username = 'sergio@orderingonlinesystem.com';
		$mail->Password = 'JYJ68utWNO15o8z1_ztrkg';
		$mail->SMTPSecure = 'tls';
		$mail->Port = 587;
		$mail->From = $rowef['value'];
		$mail->FromName = $rowsn['value'];
		$mail->addReplyTo($row['reply_to_email'], $row['reply_to_name']);
		$mail->Subject =  $lang_resource['ORDER_STATUS_TEXT'];
		$mail->AddAddress($user_mail);
		$mail->MsgHTML($msg);
		$mail->IsHTML(true);
		$mail->AltBody ="Order";
		$mail->send();
	
	if($usersfolder =='foodonq'){
			 if($ak == "Accepted"){
			 	if(file_exists('../../data/custom/'.$usersfolder.'/assigneddriver.php')){
					require_once('../../data/custom/'.$usersfolder.'/assigneddriver.php');
				}
				pg_prepare($link,'sqlsetting', "SELECT name, value FROM w_configs WHERE name='auto_assignment' OR name='auto_assignment_max_radio'");
				$result = pg_execute($link, 'sqlsetting', array());
				$settings = array();
				while ($setting = pg_fetch_array($result)) {
					$settings[$setting['name']] = $setting['value'];
				}
				if ($settings['auto_assignment'] == 1) {
					
				$autoassign = acceptOrderDriverautoAssign($link, $order, $lang_resource);
				//$autoassign = autoAssignDriver($link, $orderId);
				}
				else false;
			 }
		}
}
?>
