<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
} 
// Access-Control headers are received during OPTIONS requests
/*if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}*/
$uri =  $_SERVER["REQUEST_URI"]; //it will print full url
$uriArraylist = explode('/', $uri); //convert string into array with explode


$usersfolder = $uriArraylist[3];

/*if(!file_exists('../../../user/'.$usersfolder.'/')){
	
	header("location: http://".$_SERVER['HTTP_HOST']."/404.html");
	exit;
	}*/

require_once('../../common/settings.php');
$getData = parse_url($_SERVER['REQUEST_URI']);
$langurl = explode("&",$getData['query']);
$datakey = array();
foreach($langurl as $val) {
	$data = explode("=",$val);
	$datakey[$data[0]] = $data[1];
}
$lang='1';
$a = $datakey['a'];
$o = $datakey['o'];
$ak = $datakey['ak'];
$m = $datakey['m'];
$dt = $datakey['dt'];
$u = $datakey['u'];
$p = $datakey['p'];

$link = ConnectDB('',$usersfolder);

require_once('../../common/multilanguage.php');
require_once('../../common/ordersapp.php');

pg_prepare($link,'sqldefltlng','SELECT * FROM w_lang_setting WHERE opdefault=$1');
$dlngresult = pg_execute($link,'sqldefltlng',array('1'));
$drow = pg_fetch_array($dlngresult);
$lang = $drow['id'];

global $lang_resource;
$lang_resource = GetLangFileStaticIOSLANG($lang,$link); 

if(isset($o) && $o!="" && isset($ak) && $ak!=""){

	$link = ConnectDB('',$usersfolder);
	$order=$o;

	//fetch bus id for order
	$sqlbus = "SELECT data FROM w_orders WHERE id = $1";
	pg_prepare($link,'sqlbus',$sqlbus);
	$bus_fetch_sql = pg_execute($link,'sqlbus',array($order));

	while($bus_array = pg_fetch_array($bus_fetch_sql)){
		$bus_data = json_decode($bus_array[0]);
	}

	$rest = $bus_data->business[0]->id;
	//time zone time
	pg_prepare($link,'sqlbid',"SELECT * FROM w_orders WHERE id=$1");
	$result12 = pg_execute($link,'sqlbid',array($order));
	if(pg_num_rows($result12)>0){
		while($row12 = pg_fetch_array($result12)){
			$data= json_decode($row12['data']);
			$bid=$data->business[0]->id;
		}
		pg_prepare($link,'sqltimzone',"SELECT timezone, provider FROM w_business WHERE id=$1");
		$result13 = pg_execute($link,'sqltimzone',array($bid));
		while($row13 = pg_fetch_array($result13)){
			$timezone=$row13['timezone'];
			$provider=$row13['provider'];
		}
		$date = new DateTime('now', new DateTimeZone($timezone));
		$localtime = $date->format('h:i:s a');
		$update_date=$date->format('Y-m-d');	
	} else {
		pg_prepare($link,'sqlcontimzone',"SELECT value FROM w_configs WHERE name=$1");
		$result14 = pg_execute($link,'sqlcontimzone',array('defaulttimezone'));
		while($row14 = pg_fetch_array($result14)){
				$timezone1=$row14['value'];
		}
		//echo $timezone1;die;
		$date = new DateTime('now', new DateTimeZone($timezone1));
		$localtime = $date->format('h:i:s a');
		$update_date=$date->format('Y-m-d');
	}
	/*$time = explode("_",$m);
	if($time['0'] == ''){
		$countDown = 0;
	}
	else{
		$countDown = $time['0'];
	} */
	if(is_null($m) || $m ==''){
		$countDown = 0;
	}
	else{
		$time = explode("_",$m);
		$countDown = $time['0'];
	}
	$endTime = strtotime("+".$countDown." minutes", strtotime($localtime));
	$endTime1 = date('h:i:s a', $endTime);
	
	$nowdate = $update_date.' '.$localtime;
	$newendTime = strtotime("+".$countDown." minutes", strtotime($nowdate));
	$endTimedate = date('Y-m-d h:i:s a', $newendTime);
	//time zone time

	$sql="UPDATE w_orders SET status=$1,comment=$2,printer_stime=$3,set_time=$5,deliverytime=$6,printer_comment=$7 WHERE id=$4";
	$status=0;
	$desc="";
	$printer_stime="00:00";

	if($ak=="Rejected"){
		$status=5;
	}else if($ak=="Accepted"){
		$status=7;
	}

	if(isset($dt)){
		if($status==7){
			$desc = $lang_resource['EMAIL_PRINTER_STATUS_UPDATE_RESTAURANT'].' at :'.$endTime1;
		}else if($status==5){
			$desc=$m;
		}
		$printer_stime=$dt;
	}

	//get printer file path
	//Fetch Dynamic printer path for order business
	
	$dirPath = "/var/www/html/api/sys/user/".$usersfolder."/orders";
	$DynamicPrinterPath = "/var/www/html/api/sys/user/".$usersfolder."/orders/".$rest.".txt"; //default path
	$PathQueue = $rest;

	//1. check main settings
	pg_prepare($link,'sql_print',"SELECT * FROM w_printerpath");
	$result_print = pg_execute($link,'sql_print',array());

	while($array_print = pg_fetch_array($result_print)){
		$array_print_val = json_decode($array_print['printer_restaurant']);
		if(($array_print_val[0] == -1) || in_array($rest,$array_print_val)){ 
			//check if assigned for all or particular business
			$DynamicPrinterPath = "/var/www/html/api/sys/user/".$usersfolder."/orders/".$array_print['path'].".txt";
			$PathQueue = $array_print['path'];
			break;
		}
	}

	pg_prepare($link,'sql3',$sql);
	pg_execute($link,'sql3',array($status,$desc,$printer_stime,$order,$endTime1,$endTimedate,$m));
		//user push accept
		$credential =Pushcredential('',$usersfolder);
		$UserappType = $credential->orderingappType;
		pg_prepare($link,'sqldrvrdata','SELECT * FROM w_orders WHERE id=$1');
		$result1234 = pg_execute($link,'sqldrvrdata',array($order));
		$row123=pg_fetch_array($result1234);
		$usrId = $row123['usr'];
		pg_prepare($link,'sqlgcmuser','SELECT * FROM w_gcm WHERE login_status=1 and user_id=$1 and app_type=$2');
		$userresultgcm = pg_execute($link,'sqlgcmuser',array($usrId,$UserappType));
		$User_registatoin_ids = array();
		$User_ios_ids = array();
		while($userrowgcm=pg_fetch_array($userresultgcm)){
			array_push($User_registatoin_ids, $userrowgcm['gcm_id']);
			array_push($User_ios_ids, $userrowgcm['apns_id']);		
		}
		if($status == 7){
			 //$orderTitle = $lang_resource['PUSH_ORDER'].$order." ".$lang_resource['PUSH_ORDER_ACCEPTED_BY_BUSINESS_TIME']." at ".$dt;
			 $orderTitle = $lang_resource['EMAIL_PRINTER_STATUS_UPDATE_RESTAURANT'] .'#'.$order.' at :'.$endTime1;
			$status1= $lang_resource['EMAIL_PRINTER_STATUS_UPDATE_RESTAURANT'];
		}
		else if($status == 5){
			 $orderTitle = $lang_resource['EMAIL_PRINTER_STATUS_UPDATE_RESTAURANT_REJECT'] .'#'.$order;
			$status1 = $lang_resource['EMAIL_PRINTER_STATUS_UPDATE_RESTAURANT_REJECT'];
		}
		$response['push'] = sendUserMessage($order,$orderTitle,$status1, $User_registatoin_ids,$User_ios_ids,1,$credential->ordering_app_id,$credential->ordering_app_token);
		
		//push
	
	if(file_exists($DynamicPrinterPath)){

		/*$files = array();
		  if ($dh = opendir($dirPath)){
			while (($file = readdir($dh)) !== false){
				array_push($files,$file);
			}			
		  }*/
		

		//$files =array_diff(scandir($dirPath), array('..', '.')); //glob($dirPath.'/*.txt');
		//$files =array_map('basename', glob($dirPath . "/*.{txt}", GLOB_BRACE));
		$files =glob($dirPath.'/*.txt');
		$printer_array = array();

		foreach($files as $file){
			$valx1 = explode(".",$file);
			//echo $valx1[0];
			//$valx2 = array_shift( explode('_', $valx1[0]) );			
			$temp1   = explode('_',$valx1[0]);
			$temp2   = array_slice($temp1, 0, 3);
			//$valx2 = explode('_', $temp2);	
			//$valx2[0]=$valx2;	
			$valx2 = $temp1[0];
//echo $valx2.'$$$$$';
//echo "/var/www/html/api/sys/user/".$usersfolder."/orders/".$PathQueue.'#####';
			if($valx2 == "/var/www/html/api/sys/user/".$usersfolder."/orders/".$PathQueue){
				array_push($printer_array,$file);
			}
		}

		$file_size = count($printer_array);

		if($file_size > 1){
			unlink($DynamicPrinterPath);
			$fp = fopen($DynamicPrinterPath, "w");

			copy($printer_array[1], $DynamicPrinterPath);
			unlink($printer_array[1]);
			fclose($fp);
		}else if($file_size == 1){
			unlink($DynamicPrinterPath);
			$fp = fopen($DynamicPrinterPath, "w");
			fclose($fp);
		}
	}
	
	include_once('../../common/phpmailer/PHPMailerAutoload.php');

		//ORDER DETAILS
		$order_id = $order;
		if($ak == "Rejected"){
			$order_status = $lang_resource['ORDER_REJECTED_RESTAURANT'];//"Rejected";
		}else if($ak == "Accepted"){
			$order_status = $lang_resource['ORDER_STATUS_ACCEPTEDBYRESTAURANT'];//"Accepted";
		}
		$order_comment = $desc;

	$curyear=  date('Y');

	pg_prepare($link,'sqlconfig2ef','SELECT value FROM w_configs WHERE name=$1');
	$resultef = pg_execute($link,'sqlconfig2ef',array('sent_from_email'));
	$rowef = pg_fetch_array($resultef);

	pg_prepare($link,'sqlconfig2sn','SELECT value FROM w_configs WHERE name=$1');
	$resultsn = pg_execute($link,'sqlconfig2sn',array('sitename'));
	$rowsn = pg_fetch_array($resultsn);

	pg_prepare($link,'sqlconfig2','SELECT value FROM w_configs WHERE name=$1');
	$result5 = pg_execute($link,'sqlconfig2',array('logo_admin_header'));
	//$rowcon = pg_fetch_array($result5);
	while($row5 = pg_fetch_array($result5)){
		$logo=json_decode($row5['value']);
	}

	$serverlink = 'http://'.$_SERVER['HTTP_HOST'];
	
	$sql="SELECT data FROM w_orders WHERE id = $1";
	pg_prepare($link,'sql7',$sql);
	$res =  pg_execute($link,'sql7',array($order_id));
	$data = pg_fetch_array($res);
	$datas =   json_decode($data['data']);
	$user_mail = $datas->buyer->email;
	
	$msg = '<!doctype html>
	<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head>
					<!-- NAME: 1 COLUMN -->
					<!--[if gte mso 15]>
					<xml>
							<o:OfficeDocumentSettings>
							<o:AllowPNG/>
							<o:PixelsPerInch>96</o:PixelsPerInch>
							</o:OfficeDocumentSettings>
					</xml>
					<![endif]-->
					<meta charset="UTF-8">
					<meta http-equiv="X-UA-Compatible" content="IE=edge">
					<meta name="viewport" content="width=device-width, initial-scale=1">
					<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
					<title>Food On Q | Order Status Change</title>
					
			<style type="text/css">
			p{
				margin:10px 0;
				padding:0;
			}
			table{
				border-collapse:collapse;
			}
			h1,h2,h3,h4,h5,h6{
				display:block;
				margin:0;
				padding:0;
			}
			img,a img{
				border:0;
				height:auto;
				outline:none;
				text-decoration:none;
			}
			body,#bodyTable,#bodyCell{
				height:100%;
				margin:0;
				padding:0;
				width:100%;
			}
			.mcnPreviewText{
				display:none !important;
			}
			#outlook a{
				padding:0;
			}
			img{
				-ms-interpolation-mode:bicubic;
			}
			table{
				mso-table-lspace:0pt;
				mso-table-rspace:0pt;
			}
			.ReadMsgBody{
				width:100%;
			}
			.ExternalClass{
				width:100%;
			}
			p,a,li,td,blockquote{
				mso-line-height-rule:exactly;
			}
			a[href^=tel],a[href^=sms]{
				color:inherit;
				cursor:default;
				text-decoration:none;
			}
			p,a,li,td,body,table,blockquote{
				-ms-text-size-adjust:100%;
				-webkit-text-size-adjust:100%;
			}
			.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
				line-height:100%;
			}
			a[x-apple-data-detectors]{
				color:inherit !important;
				text-decoration:none !important;
				font-size:inherit !important;
				font-family:inherit !important;
				font-weight:inherit !important;
				line-height:inherit !important;
			}
			#bodyCell{
				padding:10px;
				border-top:1px none ;
			}
			.templateContainer{
				max-width:600px !important;
			}
			a.mcnButton{
				display:block;
			}
			.mcnImage,.mcnRetinaImage{
				vertical-align:bottom;
			}
			.mcnTextContent{
				word-break:break-word;
			}
			.mcnTextContent img{
				height:auto !important;
			}
			.mcnDividerBlock{
				table-layout:fixed !important;
			}
		
			body,#bodyTable{
				background-color:#e8e8e8;
			}
		
			#bodyCell{
				border-top:1px none ;
			}
		
			.templateContainer{
				border:0;
			}
		
			h1{
				color:#202020;
				font-family:"Lato", "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-size:26px;
				font-style:normal;
				font-weight:bold;
				line-height:125%;
				letter-spacing:normal;
				text-align:left;
			}
		
			h2{
				color:#202020;
				font-family:"Lato", "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-size:22px;
				font-style:normal;
				font-weight:bold;
				line-height:125%;
				letter-spacing:normal;
				text-align:left;
			}
		
			h3{
				color:#202020;
				font-family:"Lato", "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-size:20px;
				font-style:normal;
				font-weight:bold;
				line-height:125%;
				letter-spacing:normal;
				text-align:left;
			}
		
			h4{
				color:#202020;
				font-family:"Lato", "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-size:18px;
				font-style:normal;
				font-weight:bold;
				line-height:125%;
				letter-spacing:normal;
				text-align:left;
			}
		
			#templatePreheader{
				background-color:#e8e8e8;
				background-image:none;
				background-repeat:no-repeat;
				background-position:center;
				background-size:cover;
				border-top:0;
				border-bottom:0;
				padding-top:0px;
				padding-bottom:0px;
			}
		
			#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
				color:#ffffff;
				font-family:Helvetica;
				font-size:12px;
				line-height:150%;
				text-align:left;
			}
		
			#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
				color:#656565;
				font-weight:normal;
				text-decoration:underline;
			}
		
			#templateHeader{
				background-color:#6a96cb;
				background-image:none;
				background-repeat:no-repeat;
				background-position:center;
				background-size:cover;
				border-top:0;
				border-bottom:0;
				padding-top:0px;
				padding-bottom:0px;
			}
		
			#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
				color:#202020;
				font-family:Helvetica;
				font-size:16px;
				line-height:150%;
				text-align:left;
			}
		
			#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
				color:#2BAADF;
				font-weight:normal;
				text-decoration:underline;
			}
		
			#templateBody{
				background-color:#ffffff;
				background-image:none;
				background-repeat:no-repeat;
				background-position:center;
				background-size:cover;
				border-top:0;
				border-bottom:2px solid #EAEAEA;
				padding-top:0;
				padding-bottom:9px;
			}
		
			#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
				color:#202020;
				font-family:"Lato", "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-size:16px;
				line-height:150%;
				text-align:left;
			}
		
			#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
				color:#6a96cb;
				font-weight:normal;
				text-decoration:underline;
			}
		
			#templateFooter{
				background-color:#6a96cb;
				background-image:none;
				background-repeat:no-repeat;
				background-position:center;
				background-size:cover;
				border-top:0;
				border-bottom:0;
				padding-top:9px;
				padding-bottom:9px;
			}
		
			#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
				color:#656565;
				font-family:"Lato", "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-size:12px;
				line-height:150%;
				text-align:center;
			}
		
			#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
				color:#ffffff;
				font-weight:bold;
				text-decoration:underline;
			}
		@media only screen and (min-width:768px){
			.templateContainer{
				width:600px !important;
			}
	
	}	@media only screen and (max-width: 480px){
			body,table,td,p,a,li,blockquote{
				-webkit-text-size-adjust:none !important;
			}
	
	}	@media only screen and (max-width: 480px){
			body{
				width:100% !important;
				min-width:100% !important;
			}
	
	}	@media only screen and (max-width: 480px){
			#bodyCell{
				padding-top:10px !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnRetinaImage{
				max-width:100% !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnImage{
				width:100% !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageCardLeftImageContentContainer,.mcnImageCardRightImageContentContainer{
				max-width:100% !important;
				width:100% !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnBoxedTextContentContainer{
				min-width:100% !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnImageGroupContent{
				padding:9px !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
				padding-top:9px !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnImageCardTopImageContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
				padding-top:18px !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnImageCardBottomImageContent{
				padding-bottom:9px !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnImageGroupBlockInner{
				padding-top:0 !important;
				padding-bottom:0 !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnImageGroupBlockOuter{
				padding-top:9px !important;
				padding-bottom:9px !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnTextContent,.mcnBoxedTextContentColumn{
				padding-right:18px !important;
				padding-left:18px !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
				padding-right:18px !important;
				padding-bottom:0 !important;
				padding-left:18px !important;
			}
	
	}	@media only screen and (max-width: 480px){
			.mcpreview-image-uploader{
				display:none !important;
				width:100% !important;
			}
	
	}	@media only screen and (max-width: 480px){
		
			h1{
				font-size:22px !important;
				line-height:125% !important;
			}
	
	}	@media only screen and (max-width: 480px){
		 
			h2{
				font-size:20px !important;
				line-height:125% !important;
			}
	
	}	@media only screen and (max-width: 480px){
		
			h3{
				font-size:18px !important;
				line-height:125% !important;
			}
	
	}	@media only screen and (max-width: 480px){
		
			h4{
				font-size:16px !important;
				line-height:150% !important;
			}
	
	}	@media only screen and (max-width: 480px){
		
			.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
				font-size:14px !important;
				line-height:150% !important;
			}
	
	}	@media only screen and (max-width: 480px){
		
			#templatePreheader{
				display:block !important;
			}
	
	}	@media only screen and (max-width: 480px){
		
			#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
				font-size:14px !important;
				line-height:150% !important;
			}
	
	}	@media only screen and (max-width: 480px){
		
			#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
				font-size:16px !important;
				line-height:150% !important;
			}
	
	}	@media only screen and (max-width: 480px){
		
			#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
				font-size:16px !important;
				line-height:150% !important;
			}
	
	}	@media only screen and (max-width: 480px){
		
			#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
				font-size:14px !important;
				line-height:150% !important;
			}
	
	}</style></head>
		<body bgcolor="#fff" width="100%" style="margin: 0;" yahoo="yahoo">
			<!--*|IF:MC_PREVIEW_TEXT|*-->
			<!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Your Order Status Has Changed</span><!--<![endif]-->
			<!--*|END:IF|*-->
			<center>
			<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
					<tr>
							<td align="center" valign="top" id="bodyCell">
									<!-- BEGIN TEMPLATE // -->
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
											<tr>
													<td valign="top" id="templatePreheader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
<tbody class="mcnTextBlockOuter">
	<tr>
			<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
					<!--[if mso]>
	<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
	<tr>
	<![endif]-->
		
	<!--[if mso]>
	<td valign="top" width="600" style="width:600px;">
	<![endif]-->
					<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
							<tbody><tr>
									
									<td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px;color: #E8E8E8;">
									
											Your Order Status Has Changed
									</td>
							</tr>
					</tbody></table>
	<!--[if mso]>
	</td>
	<![endif]-->
					
	<!--[if mso]>
	</tr>
	</table>
	<![endif]-->
			</td>
	</tr>
</tbody>
</table></td>
											</tr>
											<tr>
													<td valign="top" id="templateHeader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
<tbody class="mcnImageBlockOuter">
			<tr>
					<td valign="top" style="padding:9px" class="mcnImageBlockInner">
							<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
									<tbody><tr>
											<td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0;">
													
															
																	<img align="left" alt="" src="https://gallery.mailchimp.com/449ce27bb2439ec9751d5155d/images/a5e28485-cb2e-4625-8f75-566f85c93a72.png" width="200" style="max-width: 200px; padding-bottom: 0px; vertical-align: bottom; display: inline !important; border-radius: 0%;" class="mcnImage">
															
													
											</td>
									</tr>
							</tbody></table>
					</td>
			</tr>
</tbody>
</table></td>
											</tr>
											<tr>
													<td valign="top" id="templateBody"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
<tbody class="mcnDividerBlockOuter">
	<tr>
			<td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
					<table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px none #FFFFFF;">
							<tbody><tr>
									<td>
											<span></span>
									</td>
							</tr>
					</tbody></table>
<!--            
					<td class="mcnDividerBlockInner" style="padding: 18px;">
					<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
			</td>
	</tr>
</tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
<tbody class="mcnTextBlockOuter">
	<tr>
			<td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
					<!--[if mso]>
	<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
	<tr>
	<![endif]-->
		
	<!--[if mso]>
	<td valign="top" width="600" style="width:600px;">
	<![endif]-->
					<table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
							<tbody><tr>
									
									<td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 100%;">
															<h3 class="null" style="text-align: center;">'.$lang_resource['ORDER_EMAIL_TEMPLATE_HI'].''." ".''.$datas->buyer->name.'!</h3>
															&nbsp;
															';

								$msg .= '<h3 class="null" style="text-align: center;">'.$lang_resource['PRINTER_EMAIL_THE_STATUS_CHANGE'].'</h3>';
								if($ak == "Accepted"){
									$msg .= '<h3 class="null" style="text-align: center;">'.$lang_resource['PRINTER_ORDER_ACCEPTED'].''." ".''.$order_id.'</h3>';
															 								 
									if(intval($countDown) > 0){
										$prepTime = intval($countDown);
										$delOneTime = $prepTime + 10;
										$delTwoTime = $prepTime + 20;
									}
								
								} else{
									$msg .= '<h3 class="null" style="text-align: center;">'.$lang_resource['PRINTER_ORDER_REJECTED'].''." ".''.$order_id.'</h3>';
							  }
	                           
								 $msg .= '</td>
								 </tr>
						 </tbody></table>
		 <!--[if mso]>
		 </td>
		 <![endif]-->
						 
		 <!--[if mso]>
		 </tr>
		 </table>
		 <![endif]-->
				 </td>
		 </tr>
 </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
 <tbody class="mcnDividerBlockOuter">
		 <tr>
				 <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
						 <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px none #FFFFFF;">
								 <tbody><tr>
										 <td>
												 <span></span>
										 </td>
								 </tr>
						 </tbody></table>
<!--            
						 <td class="mcnDividerBlockInner" style="padding: 18px;">
						 <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
				 </td>
		 </tr>
 </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
 <!--[if gte mso 9]>
<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
<![endif]-->
<tbody class="mcnBoxedTextBlockOuter">
		 <tr>
				 <td valign="top" class="mcnBoxedTextBlockInner">
						 
		 <!--[if gte mso 9]>
		 <td align="center" valign="top" width="300">
		 <![endif]-->
						 <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="mcnBoxedTextContentContainer">
								 <tbody><tr>
										 
										 <td class="mcnBoxedTextContentColumn" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px;">
										 
												 <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #6A96CB;">
														 <tbody><tr>
																 <td valign="top" class="mcnTextContent" style="padding: 18px;color: #FFFFFF;font-family: Lato, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;font-size: 14px;font-weight: normal;line-height: 200%;text-align: center;">
																		 <span style="color:#FFFFFF; font-size:14px; text-align:center">ESTIMATED PREPERATION TIME</span>

<h1 class="null" style="text-align: center;"><span style="color:#FFFFFF">' . $prepTime . ' mins</span></h1>

																 </td>
														 </tr>
												 </tbody></table>
										 </td>
								 </tr>
						 </tbody></table>
		 <!--[if gte mso 9]>
		 </td>
		 <![endif]-->
						 
		 <!--[if gte mso 9]>
		 <td align="center" valign="top" width="300">
		 <![endif]-->
						 <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="mcnBoxedTextContentContainer">
								 <tbody><tr>
										 
										 <td class="mcnBoxedTextContentColumn" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px;">
										 
												 <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #6A96CB;">
														 <tbody><tr>
																 <td valign="top" class="mcnTextContent" style="padding: 18px;color: #FFFFFF;font-family: Lato, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;font-size: 14px;font-weight: normal;line-height: 200%;text-align: center;">
																		 <span style="color:#FFFFFF; font-size:14px; text-align:center">ESTIMATED DELIVERY TIME</span>

<h1 class="null" style="text-align: center;"><span style="color:#FFFFFF">' . $delOneTime . '-' . $delTwoTime . ' mins</span></h1>

																 </td>
														 </tr>
												 </tbody></table>
										 </td>
								 </tr>
						 </tbody></table>
		 <!--[if gte mso 9]>
		 </td>
		 <![endif]-->
						 
		 <!--[if gte mso 9]>
						 </tr>
						 </table>
		 <![endif]-->
				 </td>
		 </tr>
 </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
 <tbody class="mcnDividerBlockOuter">
		 <tr>
				 <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
						 <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px none #FFFFFF;">
								 <tbody><tr>
										 <td>
												 <span></span>
										 </td>
								 </tr>
						 </tbody></table>
<!--            
						 <td class="mcnDividerBlockInner" style="padding: 18px;">
						 <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
				 </td>
		 </tr>
 </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
 <tbody class="mcnImageBlockOuter">
				 <tr>
						 <td valign="top" style="padding:9px" class="mcnImageBlockInner">
								 <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
										 <tbody><tr>
												 <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">
														 
																 
																		 <img align="center" alt="" src="https://gallery.mailchimp.com/449ce27bb2439ec9751d5155d/images/a3ec05ea-018d-49b4-9d05-e7d213d6d121.gif" width="200" style="max-width:200px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
																 
														 
												 </td>
										 </tr>
								 </tbody></table>
						 </td>
				 </tr>
 </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
 <tbody class="mcnDividerBlockOuter">
		 <tr>
				 <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
						 <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px none #FFFFFF;">
								 <tbody><tr>
										 <td>
												 <span></span>
										 </td>
								 </tr>
						 </tbody></table>
<!--            
						 <td class="mcnDividerBlockInner" style="padding: 18px;">
						 <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
				 </td>
		 </tr>
 </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
 <!--[if gte mso 9]>
<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
<![endif]-->
<tbody class="mcnBoxedTextBlockOuter">
		 <tr>
				 <td valign="top" class="mcnBoxedTextBlockInner">
						 
		 <!--[if gte mso 9]>
		 <td align="center" valign="top" ">
		 <![endif]-->
						 <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
								 <tbody><tr>
										 
										 <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
										 
												 <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #6A96CB;">
														 <tbody><tr>
																 <td valign="top" class="mcnTextContent" style="padding: 18px;color: #F2F2F2;font-family: Lato, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;font-size: 30px;font-style: normal;font-weight: bold;line-height: 150%;text-align: center;">
																		 Share your order on Instagram and tag us to get $9 off your next order (Free Delivery)!
																 </td>
														 </tr>
												 </tbody></table>
										 </td>
								 </tr>
						 </tbody></table>
		 <!--[if gte mso 9]>
		 </td>
		 <![endif]-->
						 
		 <!--[if gte mso 9]>
						 </tr>
						 </table>
		 <![endif]-->
				 </td>
		 </tr>
 </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
 <tbody class="mcnTextBlockOuter">
		 <tr>
				 <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
						 <!--[if mso]>
		 <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
		 <tr>
		 <![endif]-->
			 
		 <!--[if mso]>
		 <td valign="top" width="600" style="width:600px;">
		 <![endif]-->
						 <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
								 <tbody><tr>
										 
										 <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; font-family: Lato, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px;">
										 
												 Simply post a picture of your order on your feed or Insta-story and make sure to @foodon_q (otherwise we won’t see it) and we will send you a $9 voucher code (Free Delivery!) via Instagram DM.
										 </td>
								 </tr>
						 </tbody></table>
		 <!--[if mso]>
		 </td>
		 <![endif]-->
						 
		 <!--[if mso]>
		 </tr>
		 </table>
		 <![endif]-->
				 </td>
		 </tr>
 </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
 <tbody class="mcnImageBlockOuter">
				 <tr>
						 <td valign="top" style="padding:9px" class="mcnImageBlockInner">
								 <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
										 <tbody><tr>
												 <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">
														 
																 
																		 <img align="center" alt="" src="https://gallery.mailchimp.com/449ce27bb2439ec9751d5155d/images/4528c32d-60d0-4e52-811d-b8faed52c648.jpg" width="564" style="max-width:600px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
																 
														 
												 </td>
										 </tr>
								 </tbody></table>
						 </td>
				 </tr>
 </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
 <tbody class="mcnDividerBlockOuter">
		 <tr>
				 <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
						 <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px none #FFFFFF;">
								 <tbody><tr>
										 <td>
												 <span></span>
										 </td>
								 </tr>
						 </tbody></table>
<!--            
						 <td class="mcnDividerBlockInner" style="padding: 18px;">
						 <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
				 </td>
		 </tr>
 </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
 <tbody class="mcnFollowBlockOuter">
		 <tr>
				 <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
						 <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
 <tbody><tr>
		 <td align="center" style="padding-left:9px;padding-right:9px;">
				 <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
						 <tbody><tr>
								 <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
										 <table align="center" border="0" cellpadding="0" cellspacing="0">
												 <tbody><tr>
														 <td align="center" valign="top">
																 <!--[if mso]>
																 <table align="center" border="0" cellspacing="0" cellpadding="0">
																 <tr>
																 <![endif]-->
																 
																		 <!--[if mso]>
																		 <td align="center" valign="top">
																		 <![endif]-->
																		 
																				 <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked" style="display:inline;">
																							
																						 <tbody><tr>
																								 <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;">
																										 <a href="https://instagram.com/foodon_q" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-instagram-96.png" alt="Instagram" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
																								 </td>
																						 </tr>
																						 
																						 
																				 </tbody></table>
																		 
																		 
																		 <!--[if mso]>
																		 </td>
																		 <![endif]-->
																 
																		 <!--[if mso]>
																		 <td align="center" valign="top">
																		 <![endif]-->
																		 
																				 <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked" style="display:inline;">
																							
																						 <tbody><tr>
																								 <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;">
																										 <a href="https://www.facebook.com/FoodOnQltd/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-facebook-96.png" alt="Facebook" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
																								 </td>
																						 </tr>
																						 
																						 
																				 </tbody></table>
																		 
																		 
																		 <!--[if mso]>
																		 </td>
																		 <![endif]-->
																 
																		 <!--[if mso]>
																		 <td align="center" valign="top">
																		 <![endif]-->
																		 
																				 <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked" style="display:inline;">
																							
																						 <tbody><tr>
																								 <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;">
																										 <a href="https://www.foodonq.co.nz" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-link-96.png" alt="Website" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
																								 </td>
																						 </tr>
																						 
																						 
																				 </tbody></table>
																		 
																		 
																		 <!--[if mso]>
																		 </td>
																		 <![endif]-->
																 
																 <!--[if mso]>
																 </tr>
																 </table>
																 <![endif]-->
														 </td>
												 </tr>
										 </tbody></table>
								 </td>
						 </tr>
				 </tbody></table>
		 </td>
 </tr>
</tbody></table>

				 </td>
		 </tr>
 </tbody>
</table></td>
												 </tr>
												 <tr>
														 <td valign="top" id="templateFooter"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
 <tbody class="mcnTextBlockOuter">
		 <tr>
				 <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
						 <!--[if mso]>
		 <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
		 <tr>
		 <![endif]-->
			 
		 <!--[if mso]>
		 <td valign="top" width="600" style="width:600px;">
		 <![endif]-->
						 <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
								 <tbody><tr>
										 
										 <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px;color: #FFFFFF;font-family: Lato, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;font-size: 14px;line-height: 150%;text-align: center;">
										 
												 This e-mail is automatically generated. Answers to this email address will not be read. For questions please contact us.<br>
<br>
© 2018&nbsp;<a href="x-webdoc://D0B9DEC2-E74F-466F-AE9D-1CC035FE14E3">foodonq.com</a>
										 </td>
								 </tr>
						 </tbody></table>
		 <!--[if mso]>
		 </td>
		 <![endif]-->
						 
		 <!--[if mso]>
		 </tr>
		 </table>
		 <![endif]-->
				 </td>
		 </tr>
 </tbody>
</table></td>
												 </tr>
										 </table>
										 <!--[if (gte mso 9)|(IE)]>
										 </td>
										 </tr>
										 </table>
										 <![endif]-->
										 <!-- // END TEMPLATE -->
								 </td>
						 </tr>
				 </table>
		 </center>
 </body>
</html>';



		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
		$mail->PluginDir = "";
		$row = FetchAllsettingsCustomMailchmp1($link);
		$mail->isSMTP();
		$mail->Host = 'smtp.mandrillapp.com';
		$mail->SMTPAuth = true;
		$mail->Username = 'sergio@orderingonlinesystem.com';
		$mail->Password = 'JYJ68utWNO15o8z1_ztrkg';
		$mail->SMTPSecure = 'tls';
		$mail->Port = 587;
		$mail->From = $rowef['value'];
		$mail->FromName = $rowsn['value'];
		$mail->addReplyTo($row['reply_to_email'], $row['reply_to_name']);
		$mail->Subject =  $lang_resource['ORDER_STATUS_TEXT'];
		$mail->AddAddress($user_mail);
		$mail->MsgHTML($msg);
		$mail->IsHTML(true);
		$mail->AltBody ="Order";
		$mail->send();
	
	if($usersfolder =='foodonq'){
			 if($ak == "Accepted"){
			 	if(file_exists('../../data/custom/'.$usersfolder.'/assigneddriver.php')){
					require_once('../../data/custom/'.$usersfolder.'/assigneddriver.php');
				}
				pg_prepare($link,'sqlsetting', "SELECT name, value FROM w_configs WHERE name='auto_assignment' OR name='auto_assignment_max_radio'");
				$result = pg_execute($link, 'sqlsetting', array());
				$settings = array();
				while ($setting = pg_fetch_array($result)) {
					$settings[$setting['name']] = $setting['value'];
				}
				if ($settings['auto_assignment'] == 1) {
					
				$autoassign = acceptOrderDriverautoAssign($link, $order, $lang_resource);
				//$autoassign = autoAssignDriver($link, $orderId);
				}
				else false;
			 }
		}
}