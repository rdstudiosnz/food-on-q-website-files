<?php
ob_start();
// Turn off error reporting
error_reporting(0);
//header('content-type:text/html;charset=utf-8');

function convert2Digit($num){
	if($num<10){
		return $num;
	}else{
		return $num;
	}
}
$curyear=  date('Y');

pg_prepare($link,'instagramlink','SELECT value FROM w_configs where name=$1');
$resultrss3 = pg_execute($link,'instagramlink',array('instagramlink'));
$rowrss3 = pg_fetch_array($resultrss3);
$instagramlink = $rowrss3['value'];

$sqldformat = "SELECT * from w_configs WHERE  name='siteurl'";
pg_prepare($link,'sqlsiteformat',$sqldformat);
$resultdformat = pg_execute($link,'sqlsiteformat',array());
$rowdformat = pg_fetch_array($resultdformat);
$siteurl=$rowdformat["value"];

$sqldformat1 = "SELECT * from w_configs WHERE  name='sitename'";
pg_prepare($link,'sqlsiteformat1',$sqldformat1);
$resultdformat1 = pg_execute($link,'sqlsiteformat1',array());
$rowdformat1 = pg_fetch_array($resultdformat1);
$sitname=$rowdformat1["value"];
		
if($order->StockQty){
	$data=$order->StockQty;
	foreach($data as $val){
	pg_prepare($link,'sqld4578'.$val->id,'SELECT * FROM w_dishes WHERE id=$1');
	$resulttSto = pg_execute($link,'sqld4578'.$val->id,array($val->id));	
	$rSto = pg_fetch_array($resulttSto);
	
	$fSto = ($rSto['stock_qty'])-($val->value);
	
		
	pg_prepare($link,'sqld45'.$val->id,'UPDATE w_dishes SET stock_qty=$1 WHERE id=$2');
	pg_execute($link,'sqld45'.$val->id,array($fSto,$val->id));
	}
}


$particular_business = $order->business[0];
if ($particular_business->paymethod->paypal==true) {
    if(isset($tx))
	{
     $paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYPAL']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE'].":" . $tx . ")";
	}
	else
	{
		 $paymethod_paypal = "Order Placed (Pending Payment)";
	}
}
if ($particular_business->paymethod->cash==true) {
     $paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_CASH'];
}

if ($particular_business->paymethod->authorize==true) {
	 //$paymethod_paypal = "Paid via Authorize.net (Transaction Code " . $a_trnx_code . ")";
	 $paymethod_paypal = $lang_resource['V3_ORDER_PAID_AUTHORIZE']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $a_trnx_code . ")";
}


if ($particular_business->paymethod->braintree==true) {
	 //$paymethod_paypal = "Paid via BrainTree (Transaction Code " . $paymentid . ")";
	 $paymethod_paypal = $lang_resource['V3_ORDER_PAID_BRAIN']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";
}


if ($particular_business->paymethod->transactium==true) {
	if($paymentid=='Failure'){
		$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_NOT_TRANSACTIUM'];
	}else{
		//$paymethod_paypal = "Paid via Transactium (Transaction: " . $paymentid . ")";
		$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_TRANSACTIUM']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";	
	}	
}
if ($particular_business->paymethod->pexpress==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYMENTEXPRESS']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";	
}

if ($particular_business->paymethod->maksekeskus==true) {
	//$paymethod_paypal = "Paid via Maksekeskus (Transaction Code " . $paymentid . ")";
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_MAKSEKESKUS']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";	
}
if ($particular_business->paymethod->voguepay==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_VOGUEPAY']." "."(" . $paymentid . ")";
}

if ($particular_business->paymethod->skrill==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_SKRILL']." "."(" . $paymentid . ")";
}

if ($particular_business->paymethod->payeezy==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYEEZY']." "."(" . $paymentid . ")";
}
if ($particular_business->paymethod->payu==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYU']." "."(" . $paymentid . ")";
}
if ($particular_business->paymethod->stripe==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_STRIPE']." "."(" . $paymentid . ")";
}
if ($particular_business->paymethod->paypalpro==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYPALPRO']." "."(" . $paymentid . ")";
}
if ($particular_business->paymethod->paygistix==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYGISTIX']." "."(" . $paymentid . ")";
}
if ($particular_business->paymethod->global==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_GLOBAL']." "."(" . $paymentid . ")";
}
if ($particular_business->paymethod->btrans==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_BTRANS']." "."(" . $paymentid . ")";
}
if ($particular_business->paymethod->bsa==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_BSA']." "."(" . $paymentid . ")";
}
if ($particular_business->paymethod->azul==true) {
	$paymethod_paypal = $lang_resource['PAYMENT_GATEWAY_ALL_AZUL_PAYMENT_SUCCESS']." "."(" . $paymentid . ")";
}
if ($particular_business->paymethod->quickpay==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_QUICKPAY']." "."(" . $paymentid . ")";
}
if ($particular_business->paymethod->paynl==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYNL']." "."(" . $paymentid . ")";
}
if ($particular_business->paymethod->zaakpay==true) {
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_ZAAKPAY']." "."(" . $paymentid . ")";
}
//request collection start
 if((isset($requestcollectionset))&&($requestcollectionset==true)){
	$paymethod_paypal = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYPAL']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $tx . ")";
	$deltype=$lang_resource['FRONT_VISUAL_REQUEST_COLLECTION'];
 }
//request collection end 

if(!$sendemail){
	//check if paymethod  cash or card
	if(($particular_business->paymethod->cash==false) && (($particular_business->paymethod->card==false)) && ($particular_business->paymethod->paypal==false)){
		$relative_printer_path = "../../../orders/"; 
		include_once "../../lib/printer-code.php";
	}
}



$Showmsg = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, text-size=normal">
<title>'.$_SERVER["HTTP_HOST"].' : '.$lang_resource['INVOICE_ORDER_PDF'].' # ' . $id . '</title>
<link rel="stylesheet" type="text/css" href="../../templates/style.css"/>
<style type="text/css">
body, td, input, textarea, select{
	margin:0;
	font-family: arial,sans-serif;
}
.message1{
	font-family:Open Sans, Arial, Helvetica, sans-serif;font-size:30px;color:#404040;	
}
.message2{
	color:#e74c3c;font-size:24px;font-weight:bold;font-family:Open Sans, Arial, Helvetica, sans-serif;	
}
.message3{
	font-family:Open Sans, Arial, Helvetica, sans-serif;font-size:20px;color:#000;text-align:center;		
}
.heading td{
	font-family:Open Sans, Arial, Helvetica, sans-serif;font-size:14px;color:#fff;font-weight:bold;
}
.even{
	background-color:#f7f7f7;
}
.odd{
	background-color:#ebebeb;
}
.detail-table{
	border:solid 1px #e3e3e3;
	font-family:Open Sans, Arial, Helvetica, sans-serif;font-size:15px;color:#666666;
	margin-top:15px;	
}
.detail-table tr td{
	border-bottom:solid 2px #fff;
	padding:8px;	
}
.total-price{
	width:100%;text-align:right;	
	font-family:Open Sans, Arial, Helvetica, sans-serif;font-size:16px;color:#000;font-weight:bold;display:block;float:left;
}
.message4{
	text-align:center;
	font-family:Open Sans, Arial, Helvetica, sans-serif;font-size:16px;color:#666666;
	border-bottom:solid 1px #dedede;
}
.app-table{
	margin-top:10px;	
}
.footer-table{
	background-color:#252a30;
	height:44px;
	margin-top:15px;	
	font-family:Open Sans, Arial, Helvetica, sans-serif;font-size:10px;color:#fff;
	text-align:center;
}
.footer-table td a{
	color:#fff;
	text-decoration:none;
}
ul {
	list-style: none;
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	width: 100%;
	float:left;
	
	}
li{
font-size: 12px;
color: #333;

line-height: 17px;
}
 h4{
margin: 5px 0px 0px 0px;
padding: 0px 0px 0px 0px;
font-size: 12px;
color: #e74c3c;
text-transform: uppercase;
}
.options{
	width:100%;
	float:left;
	margin: 7px 0px 0px 0px;
padding: 0px 0px 0px 5px;
}

.gray_button{
    border-radius:3px;
    width:27px;
    min-height:18px;
    float:left; 
    border:none; 
    margin:0px 0px 0px 0px; 
    background:#dedede; 
}
.green_button{
    border-radius:3px; 
    width:27px; 
    min-height:18px; 
    float:left; 
    background:#91d547; 
    border:none; 
    margin:0px 0px 0px 0px; 
}
h5 {
 background:#dedede;
 padding:14px;
 margin:0 5px 5px;
 border-radius:5px;
 float:left;
 width:0px !important;
 height:0px !important;
 
}
h6 {
 background:#91d547;
 padding:14px;
 margin:0 5px 5px;
 border-radius:5px;
 width:0px !important;
 height:0px !important;
 float:left;
 
}
.copyr8 a {
	color:#e4981e;
}
</style>

</head>

<body>

    <div class="container">
        
        <div class="tab1">
        <div class="back-sec">
			<a href="http://'.$_SERVER["HTTP_HOST"].'" style="text-decoration: none;"><button type="button" class=" back_btn">'.$lang_resource['SHOPPING_SECOND_WHERE_BACK_BUTTON'].'</button></a>
        </div><!--back-sec-->
            	
            <div class="top-sec">
                
                <div class="logo">
                    <a href="http://' . $_SERVER["HTTP_HOST"] . '">';
					if($_SESSION['scriptid']=='0' || $_SESSION['scriptid']=='')
					{
                      $Showmsg .=   '<img src="http://'.$_SERVER['HTTP_HOST'].'/panel/images/logo/1/normal.jpg" alt="logo"/>';
					}
					else
					{
						 $Showmsg .=   '<img src="http://'.$_SERVER['HTTP_HOST'].'/panel/images/logo/1/'.$_SESSION['scriptid'].'/normal.jpg" alt="logo"/>';
					}
                   $Showmsg .=  '</a>
                </div>                
            </div>
                    
            <div class="tab2">
                
                <div class="msg">
                    <div>
                    '.$lang_resource['FRONT_MAIN_EMAIL_HI_THERE'].',<br/>
                   '.$lang_resource['FRONT_MAIN_EMAIL_ORDER_PLACED_SUCCESS'].'
                    </div>	
                </div>';

                 $bu_id=$particular_business->id; // particular Business id
               pg_prepare($link,'sql5b','SELECT * from w_business WHERE id=$1');
               $result23b = pg_execute($link,'sql5b',array($bu_id));
               $row23b = pg_fetch_array($result23b);
			  $row23b['currency'] = currency_symbol($row23b['currency']);
			   
			   
			 
			   
			  // include_once "../lib/front_bulk.php";
			  //$row23b['currency'] = currency_symbol($row23b['currency']);
			  
			
			   
               if ($row23b['isimg']==1){ // Check image is upload for particular Business
                 $bb_img='http://' . $_SERVER["HTTP_HOST"].'/panel/images/business/'.$row23b['id'].'/original.jpg';
               }else{
                 $bb_img='http://' . $_SERVER["HTTP_HOST"].'/panel/images/business/dummy.jpg';  
               }  
			    pg_prepare($link,'sql5cx','SELECT city from w_franchises WHERE id=$1');
               $resultcity = pg_execute($link,'sql5cx',array($row23b['city']));
  			   $cityfetch = pg_fetch_array($resultcity); 

  			   pg_prepare($link,'sql5country','SELECT name from w_countries WHERE id=$1');
               $resultcountry = pg_execute($link,'sql5country',array($row23b['country']));
               $countryfetch = pg_fetch_array($resultcountry); 

			  
               $Showmsg .='<div class="brand-logo">
                    <img src="'.$bb_img.'">
                </div>
                
                <div class="order-misc">
                    <div class="ord-msg">
                        <div class="msg-text1">'.$lang_resource['Order_V2'].' # ' . $id . '</div>
                        <a href="http://' . $_SERVER["HTTP_HOST"] . '">'.$lang_resource['MOBILE_FRONT_VISUALS_TRACK_ORDER'].'</a>
                    </div>';
					if((!isset($requestcollectionset))||($requestcollectionset==false)){
                    $Showmsg .=' <div class="msg-text">
                        <div>'.$lang_resource['MOBILE_MENU_LIST_NEED_CHANGE_ON_YOUR_ORDER'].'<br/>
                            <span class="sp1">'.$particular_business->name.' : <strong>'.$particular_business->tel.'</strong></span>
                        </div>
                    </div>';
					}else{
							 $Showmsg .='<div class="msg-text">
                        <div>'.$lang_resource['MOBILE_MENU_LIST_NEED_CHANGE_ON_YOUR_ORDER'].'<br/>
                            <span class="sp1">'.ucfirst($requestcollectiondata[0]["resturent_name"]).' : <strong>'.$requestcollectiondata[0]["resturent_contactno"].'</strong></span>
                        </div>
                    </div>';
							
              }
                $Showmsg .='  </div>
                
            </div>';
			
            if($order->buyer->deliveryType == "pickup") {	

            	$resturantaddress = ucfirst($row23b['street']) .','. ucfirst($row23b['colony']) .','. $cityfetch['city'].' , '. $countryfetch['name'];
                $resturantaddress = str_replace("/","@",$resturantaddress);
				
				
				$addr = str_replace("%20", " ", $order->buyer->address);
				$addrs = str_replace("%2C", " ", $addr);
            	$buyeraddress = ucfirst($addrs).','. $order->buyer->colony . ','. $order->buyer->cityname ;
                $buyeraddress = str_replace("/","@",$buyeraddress);
					
				$Showmsg .='<table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table">
                <tr bgcolor="e74c3c">
                    <td colspan="2" style="border-bottom:solid 2px #fff;font-family:Open Sans, Arial, Helvetica, sans-serif;font-size:14px;color:#fff;font-weight:bold;">'.$lang_resource['EMAIL_RESTAURENT_INFO'] .'</td>
                  
                </tr>
                <tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['EMAIL_RESTAURENT_NAME'].' : </td>
                    <td>' .str_replace("%20", " ", ucfirst($row23b['name'])). '</td>
                </tr>
                <tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['EMAIL_RESTAURENT_ADDRESS'].' : </td>
                     <td>' .str_replace("%20", " ", ucfirst($row23b['street'])) . '</td>
                </tr>
                <tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['EMAIL_RESTAURENT_NEIGHBORHOOD'].' : </td>
                    <td>' . str_replace("%20", " ", ucfirst($row23b['colony'])) . '</td>
                </tr>
                <tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['EMAIL_RESTAURENT_CITY'].' :  </td>
                    <td>' .  $cityfetch['city'] . '</td>
                </tr>
                <tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['EMAIL_RESTAURENT_ZIPCODE'].' : </td>
                    <td>' .str_replace("%20", " ", ucfirst($row23b['cp'])) . '</td>
                </tr>
                <tr bgcolor="#f7f7f7">
 					<td colspan="2" align="center"><a href="https://www.google.co.in/maps/dir/'.$resturantaddress.'/'.$buyeraddress.'" class="getdirection">Get Direction of Takeway</a></td>
				</tr>
               
			</table>';
	}



			 
                    
            $Showmsg .='<table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table">';
			
			if($braintree_firstname){ //If paid by Braintree
				$Showmsg .='<tr>
				<td><div class="message3" style="text-align:left;width:300px;">'.$order->buyer->deliveryType.' Address</div></td>
				</tr>';
			}
			if($order->business[0]->dishes){  
                $Showmsg .='<tr bgcolor="e74c3c">
                    <td width="30%" class="top-td1">'.$lang_resource['PAYMENT_GATEWAY_ALL_DESCRIPTION'].'</td>
                    <td class="top-td2">'.$lang_resource['FRONT_MAIN_EMAIL_USER_DETAILS'].'</td>
                </tr>';
                
                $checkoutfields = $order->buyer->checkoutfields;
				
              if((!isset($requestcollectionset))||($requestcollectionset==false)){
				if(in_array('Name', $checkoutfields)){
                $Showmsg .='<tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['MOBILE_CHECKOUT_NAME'].'</td>';
				
						  $Showmsg .='  <td>' . ucfirst(str_replace("%20", " ", $order->buyer->name)) . '</td>';
					
                   
                 $Showmsg .='  </tr>';
				}
				}else{
					  $Showmsg .='<tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['MOBILE_CHECKOUT_NAME'].'</td>';
					  $Showmsg .='  <td>' . ucfirst(str_replace("%20", " ", $requestcollectiondata[0]["customer_name"])) . '</td>';
					    $Showmsg .='  </tr>';
				}
			if((!isset($requestcollectionset))||($requestcollectionset==false)){
				if(in_array('Last Name', $checkoutfields)){
				$Showmsg .='<tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['MOBILE_CHECKOUT_LASTNAME'].'</td>
                    <td>' . str_replace("%20", " ", ucfirst($order->buyer->lastname2)) . '</td>
                </tr>';
				}
				if(in_array('Email', $checkoutfields)){
                $Showmsg .='<tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['MOBILE_FRONT_VISUALS_EMAIL'].'</td>
                    <td><a class="mail-id" target="_blank" href="mailto:' . strtolower($order->buyer->email) . '">' . strtolower($order->buyer->email) . '</a></td>
                </tr>';
				}
			 }
			 	  if((!isset($requestcollectionset))||($requestcollectionset==false)){
				if(in_array('Full Address', $checkoutfields)){
				$addr = str_replace("%20", " ", $order->buyer->address);
				$addrs = str_replace("%2C", " ", $addr);
                $Showmsg .='<tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['FRONT_MAIN_EMAIL_HOME_ADDRESS'].' </td>
                    <td>' . ucfirst($addrs) . '</td>
                </tr>';
				}
				if(in_array('APT/Suit', $checkoutfields)){
				 $Showmsg .='<tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['MOBILE_FRONT_VISUALS_APT'].' </td>
                    <td>' . $order->buyer->api . '</td>
                </tr>';
				}
				
				
				if(in_array('Where did you find about us', $checkoutfields)){
                $Showmsg .='<tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['Referenece_V2'].' </td>
                    <td>' . $order->buyer->reference . '</td>
                </tr>';
            }
			
				if(in_array('Phone', $checkoutfields)){
                $Showmsg .='<tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['MOBILE_CHECKOUT_PHONE'].'</td>
                    <td>' . $order->buyer->tel . '</td>
                </tr>';
				}
				
				if(in_array('City', $checkoutfields)){
                $Showmsg .='<tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['MOBILE_FRONT_VISUALS_CITY'].'</td>
                    <td>' . $order->buyer->cityname . '</td>
                </tr>';
				}
				if(in_array('Area / Neighborhood', $checkoutfields)){
				 $Showmsg .='<tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['MOBILE_FRONT_VISUALS_NEIGHBOR'].'</td>
                    <td>' . str_replace("%20", " ", $order->buyer->colony) . '</td>
                </tr>';
				}
				if(in_array('Zipcode', $checkoutfields)){
				$Showmsg .='<tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['MOBILE_FRONT_VISUALS_ZIP'].'</td>
                    <td>' . $order->buyer->zipcode . '</td>
                </tr>';
				}
				  }else{
					    $Showmsg .='  <tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['FRONT_MAIN_EMAIL_HOME_ADDRESS'].' </td>
                    <td>' . ucfirst($requestcollectiondata[0]["customer_address1"].','.$requestcollectiondata[0]["customer_address2"]) . '</td>
                </tr>';
				   $Showmsg .='   <tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['MOBILE_FRONT_VISUALS_CITY'].'</td>
                    <td>' . $requestcollectiondata[0]["customer_town"] . '</td>
                </tr>'; $Showmsg .='   <tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['REQUEST_COLLECTION_CUSTOMER_POSTERCODE'].'</td>
                    <td>' . $requestcollectiondata[0]["customer_postcode"] . '</td>
                </tr>';
				
				  $Showmsg .='   <tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['MOBILE_CHECKOUT_PHONE'].'</td>
                    <td>' .$requestcollectiondata[0]["customer_contactno"] . '</td>
                </tr>';
				 $Showmsg .='   <tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['REQUEST_COLLECTION_CUSTOMER_NOTES'].'</td>
                    <td>' . $requestcollectiondata[0]["customer_note"] . '</td>
                </tr>';
				  }
			
				 if((!isset($requestcollectionset))||($requestcollectionset==false)){
				if ($order->preorder == 'true') {
						  //Time selection settings. 
							   $str='';
							$time_format=$rowtimeformat['value'];
							if($time_format==12){
								
									$str='PM';
									if($order->preordertimehh<12){
										$str='AM';
									}
									$order->preordertimehh=floor($order->preordertimehh%12);
									
									 if (intval($order->preordertimehh) < 10) {
										$order->preordertimehh = "0".$order->preordertimehh;
									}
									if (intval($order->preordertimemm) < 10) {
										$order->preordertimemm = "0".$order->preordertimemm;
									}
							}else{
								
								if($order->preordertimehh>=24){
										 $str='AM';
										$order->preordertimehh=floor($order->preordertimehh%12);
										
									}
									 if (intval($order->preordertimehh) < 10) {
										$order->preordertimehh = "0".$order->preordertimehh;
									}
									if (intval($order->preordertimemm) < 10) {
										$order->preordertimemm = "0".$order->preordertimemm;
									}
							}
							
				if(in_array('Takeout Date', $checkoutfields)){		
                $Showmsg .= '<tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['FRONT_MAIN_EMAIL_ORDER_TYPE'].'</td>
                    <td><strong>'.$lang_resource['SHOPPING_PREORDER'].'</strong></td>
                </tr>
                <tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['V3_ORDER_DELIVERY_DATE'].' &amp; '.$lang_resource['RESPONSIVE_ORDER_EMAIL_TIME'].'</td>
				
                    <td>' .$order->buyer->deliverydate . '  ' . $order->preordertimehh . ': '. $order->preordertimemm .' '.$str. '</td>
                </tr>';
				}
				else {
					if($order->buyer->deliverydate) {
						
						if($order->buyer->deliverydate == "ASAP")
						$dtxts = $lang_resource['V3_ORDER_DELIVERY_TIME'];
						else 
						$dtxts = $lang_resource['V3_ORDER_DELIVERY_DATE'];
						
				  $Showmsg .= '<tr bgcolor="#f7f7f7">
                    <td>'.$dtxts.'</td>
                    <td><strong>'.$order->buyer->deliverydate.'</strong></td>
                </tr>';
				}
				if($order->buyer->deliveryhours) {
				$Showmsg .= '<tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['V3_ORDER_DELIVERY_TIME'].'</td>
                    <td>'.$order->buyer->deliverytimeText.'</td>
                </tr>';		
				}
		}
	}
				 }
				 $htime = date("H",strtotime($requestcollectiondata[0]["resturent_collection_time"]));
				  $mtime = date("i",strtotime($requestcollectiondata[0]["resturent_collection_time"]));
				 $time_format=$rowtimeformat['value'];
				 $str='';
							if($time_format==12){
								
									$str='PM';
									if($htime<12){
										$str='AM';
									}
									$htime=floor($htime%12);
									
									 if (intval($htime) < 10) {
										$htime = "0".intval($htime);
									}
									if (intval($mtime) < 10) {
										$mtime = "0".intval($mtime);
									}
							}else{
								
								if($htime>=24){
										 $str='AM';
										$htime=floor($htime%12);
										
									}
									 if (intval($htime) < 10) {
										$htime = "0".intval($htime);
									}
									if (intval($mtime) < 10) {
										$mtime= "0".intval($mtime);
									}
							}
				  $date = date("d/m/Y",strtotime($requestcollectiondata[0]["resturent_collection_time"]));
            $Showmsg .= '</table>';
			  if((isset($requestcollectionset))&&($requestcollectionset==true)){
				 $Showmsg .= '   <table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table">
                <tr bgcolor="e74c3c">
                    <td width="30%" class="top-td1">Description</td>
                    <td class="top-td2">'.$lang_resource['FRONT_MAIN_EMAIL_RESTRENT_DETAILS'].'</td>
                </tr> <tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['MOBILE_CHECKOUT_NAME'].'</td>';
					 $Showmsg .='  <td>' . ucfirst($requestcollectiondata[0]["resturent_name"]) . '</td>';
					   $Showmsg .='  </tr>';
					     $Showmsg .='  <tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['REQUEST_COLLECTION_RESTURENT_COLLECTION_TIME'].' </td>
					
                    <td>' . $date.'&nbsp;'.$htime.":".$mtime ."&nbsp;".$str.'</td>
                </tr>';
				  $Showmsg .='   <tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['REQUEST_COLLECTION_RESTURENT_OTHER_VALUE'].'</td>
                    <td>' .$requestcollectiondata[0]["resturent_other_value"] . '</td>
                </tr>';
					 $Showmsg .='   <tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['REQUEST_COLLECTION_RESTURENT_OTHER_REFERENCE'].'</td>
                    <td>' . $requestcollectiondata[0]["resturent_other_reference"] . '</td>
                </tr>';   
					 $Showmsg .='   <tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['REQUEST_COLLECTION_CUSTOMER_POSTERCODE'].'</td>
                    <td>' . $requestcollectiondata[0]["resturent_postcode"] . '</td>
                </tr>';      
					   
				  $Showmsg .= '</table>';
				
			  }
			 if((!isset($requestcollectionset))||($requestcollectionset==false)){
			//Braintree section
			if($braintree_firstname)
					{
						
						if($order->buyer->address!=$braintree_address1)
						{
			
			$Showmsg .= '<table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table">
               <tr>
				<td><div class="message3" style="text-align:left;width:300px;">'.$lang_resource['PAYMENT_BRAINTREE_BILL_ADDRESS'].'</div></td>
				</tr>
                <tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['CONTROL_PANEL_BRAINTREE_FIRSTNMAE'].'</td>
                    <td>' .  $braintree_firstname .  '</td>
                </tr>
				
				<tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['MOBILE_CHECKOUT_LASTNAME'].'</td>
                    <td>' . $braintree_lastname . '</td>
                </tr>
				
                <tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['CONTROL_PANEL_BRAINTREE_ADDRESS1'].'</td>
                    <td>' . $braintree_address1 . '</td>
                </tr>
                <tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['CONTROL_PANEL_BRAINTREE_ADDRESS2'].' </td>
                    <td>' . $braintree_address2 . '</td>
                </tr>
				
				 <tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['CONTROL_PANEL_BRAINTREE_CITY'].' </td>
                    <td>' . $braintree_city . '</td>
                </tr>
				
				
                <tr bgcolor="#ebebeb">
                    <td>'.$lang_resource['CONTROL_PANEL_BRAINTREE_STATE'].' </td>
                    <td>' . $braintree_state . '</td>
                </tr>
                <tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['CONTROL_PANEL_BRAINTREE_ZIPCODE'].'</td>
                    <td>' . $braintree_zipcode . '</td>
                </tr>
                
				
				
				';
					
            $Showmsg .= '</table>';
			
						}
						else
						{
							$Showmsg .='<table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table" style="margin-top:15px;">
                    	<tr>
				<td><div class="message3" style="text-align:left;width:300px;">'.$lang_resource['PAYMENT_BRAINTREE_BILL_ADDRESS_SAME']." ".$order->buyer->deliveryType." ".$lang_resource['PAYMENT_BRAINTREE_BILL_ADDRESS_SAME_ADDRESS'].'</td>
				</tr>
							
						 </table>';	
						}
			
					}
			//end braintree section
                  
           $Showmsg .= ' <table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table">
                <tr bgcolor="e74c3c" class="heading">				
                    <td class="top-td1">'.$lang_resource['FRONT_MAIN_EMAIL_ITEMS'].'</td>
					<td class="top-td1">'.$lang_resource['SHOPPING_NO_OF_PERSON'].'</td>
                    <td class="top-td1">'.$lang_resource['FRONT_MAIN_EMAIL_OPTIONS'].'</td>
                    <td class="top-td1">'.$lang_resource['FRONT_MAIN_EMAIL_COMMENT'].'</td>
                    <td class="top-td1">'.$lang_resource['FRONT_MAIN_EMAIL_RATE'].'</td>
                </tr>';
		
		foreach ($particular_business->dishes as $dish) {
			if($dish->options) {
			 $productOptionHtml =  Margeslash($dish->options);  
			} else {
				$productOptionHtml ='';
				}
			 
                $Showmsg .= '<tr bgcolor="#f7f7f7">
                    <td width="35%">' .$dish->quantity.' x ' . $dish->name . '</td>
					<td width="35%">' .$dish->nofperson. '</td>
                    <td width="25%">'. $productOptionHtml .'</td>
					
                    <td width="20%">' . str_replace("%20", " ", ucfirst($dish->comments)) . '</td>
                    <td width="40%">'.$row23b["currency"].' '. convert2Digit($dish->total) . '</td>
                </tr>';
				 $total = $total + $dish->total;
        }
				
		if ($order->buyer->taxtype == 1) {
			$total     = $total + $particular_business->shipping + $order->tax;
			$taxstring = $lang_resource['FRONT_MAIN_EMAIL_TAX_NOT_INCLUDED'];
		} else {
			$total     = $total + $particular_business->shipping;
			$taxstring = $lang_resource['FRONT_MAIN_EMAIL_TAX_INCLUDED'];
		}
		
		
		
		$taxpercentage = $order->buyer->tax;
		$tipsprice     = $order->buyer->tips;
		
		if ($order->buyer->tips > 0) {
			$total = $total + $order->buyer->tips;
		}
		
		$total = $total;
		
		$deltype = $order->buyer->deliveryType;
		
		//business shipping and comment info
		if ($particular_business->shipping == '0.00')
			$shippingcaption = $lang_resource['FRONT_MAIN_HOME_DELIVERY'];
		else
			$shippingcaption = $lang_resource['FRONT_MAIN_HOME_DELIVERY'];

				if($deltype != "Pickup") {	

   $Showmsg .= '<tr bgcolor="#ebebeb">
                    <td>' . $shippingcaption . '</td>
                    <td>&nbsp;</td>
					<td>&nbsp;</td>
                    <td>' . ucfirst(strtolower($particular_business->comments)) . '</td>
                    <td>'.$row23b["currency"].' '. convert2Digit($particular_business->shipping ). '</td>
                </tr>';
				       }
				
                        /*discount code section */
                         if((isset($order->discountcategory)) && (trim($order->discountprice) !=''))
		  {
                            if ($order->discounttype == 1)
                                $discountcaption = $lang_resource['SHOPPING_DISCOUNT_TEXT'] . " (" . $order->discountrate . "%)";
                            else if ($order->discounttype == 2)
                                $discountcaption = $lang_resource['SHOPPING_DISCOUNT_TEXT'];
                            
                            $total = $total - $order->discountprice;
                            $total = $total;
                            
           if(in_array('Discount Coupon', $checkoutfields)){	             
  			 $Showmsg .= '<tr bgcolor="#ebebeb">
                    <td>' . $discountcaption . '</td>
                    <td>&nbsp;</td>
					<td>&nbsp;</td>

                    <td>' . ucfirst(strtolower($order->discountcomments)) . '</td>
                    <td>'.$row23b["currency"].' '.convert2Digit($order->discountprice) . '</td>
                </tr>';
            }
			}
			
			
			
			
			
			
                  /*service fee section */
		  if(isset($order->servicefeeTotal1))
		  {
			 if(($order->servicefee==null) ||($order->servicefee=='')){
				 $order->servicefee=0;
			 }
			   $discountcaption = $lang_resource['SERVICE_FEE_V2'] ." (" .$order->servicefee ."%)";
			  
			 if($cntr % 2 == 0){ $bg = '#f7f7f7'; }else{ $bg = '#ebebeb'; }		
			$Showmsg .='<tr bgcolor="'.$bg.'">
					  <td style="border-bottom:solid 2px #fff;padding:8px;">' . $discountcaption . '</td>
					  <td style="border-bottom:solid 2px #fff;padding:8px;">&nbsp;</td>
					  <td style="border-bottom:solid 2px #fff;padding:8px;">&nbsp;</td>
					  <td style="border-bottom:solid 2px #fff;padding:8px;">&nbsp;</td>
					  <td style="border-bottom:solid 2px #fff;padding:8px;">'.$row23b["currency"].' '. convert2Digit($order->servicefeeTotal1) . '</td>
				  </tr>';
				  $cntr ++;
		  }
		  /*service fee section */        
                        /*discount code section */
				
				
			  if ($order->tax > 0) {
  			 $Showmsg .= '<tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['REORDER_CONFIRM_TAX'].' (' . $taxpercentage . '%)<br/> <span style="font-size:11px;font-weight:bold">' . ucfirst(strtolower($taxstring)) . '</span></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
					<td>&nbsp;</td>
                    <td>'.$row23b["currency"].' '. convert2Digit($order->tax) . '</td>
                </tr>';
				
			  }
			  if(in_array('Tip For The Driver', $checkoutfields)){	
			  
			  if ($order->buyer->tips > 0) {
				$Showmsg .= '<tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['CHECKOUT_TIP_FOR_THE_DRIVER'].'</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
					<td>&nbsp;</td>
                    <td>'.$row23b["currency"].' '.convert2Digit($tipsprice) . '</td>
                </tr>';

			  }
			}
						  
				
	$Showmsg .= '</table>';

		 }else{
				   $Showmsg .= ' <table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table">';
				    $Showmsg .= '<tr bgcolor="#f7f7f7">
                    <td>'.$lang_resource['REQUEST_COLLECTION_DELIVERY_FEE'].'</td>
                   
                    <td>' . $requestcollectiondata[0]["deliveryprice"] . '</td>
                </tr>';
				 $total = $total +$requestcollectiondata[0]["deliveryprice"];
				      $Showmsg .= ' </table>';
			 }
			 }
			 
			  if(!$order->business[0]->dishes){						
					 $Showmsg .='<table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table" style="margin-top:15px;">
                    	<tr bgcolor="e74c3c">
                        	<td width="30%" style="border-bottom:solid 2px #fff;font-family:Open Sans, Arial, Helvetica, sans-serif;font-size:14px;color:#fff;font-weight:bold;">'.$lang_resource['CONTROL_PANEL_BUSINESS_EXTRAS_DESCRIPTION_HEADER'].'</td>
                            <td style="border-bottom:solid 2px #fff;font-family:Open Sans, Arial, Helvetica, sans-serif;font-size:14px;color:#fff;font-weight:bold;">'.$lang_resource['FRONT_MAIN_EMAIL_USER_DETAILS'].'</td>
                        </tr>
                        <tr bgcolor="#f7f7f7">
                        	<td style="border-bottom:solid 2px #fff;padding:8px;">'.$lang_resource['LOGIN_CREATE_NAME'].'</td>
                            <td style="border-bottom:solid 2px #fff;padding:8px;">'. ucfirst(str_replace("%20", " ", $order->buyer->name)) . '</td>
                        </tr>
                        <tr bgcolor="#ebebeb">
                        	<td style="border-bottom:solid 2px #fff;padding:8px;">'.$lang_resource['LOGIN_CREATE_EMAIL'].'</td>
                            <td style="border-bottom:solid 2px #fff;padding:8px;"><a style="color:#666666;text-decoration:none;" target="_blank" href="mailto:'. strtolower($order->buyer->email) . '">'. strtolower($order->buyer->email) . '</a></td>
                        </tr>
                        
                        <tr bgcolor="#f7f7f7">
                        	<td style="border-bottom:solid 2px #fff;padding:8px;">'.$lang_resource['Phone_V2'].'</td>
                            <td style="border-bottom:solid 2px #fff;padding:8px;">'. $order->buyer->tel . '</td>
                        </tr>
                		</table>';
						
					}	
       if($order->reservestatus){
						$reservequery = 'SELECT * from w_reserve WHERE  business=$1';
						pg_prepare($link,'sqlshowr2',$reservequery);
						$reserverecord = pg_execute($link,'sqlshowr2',array($order->business[0]->id));
					
						$reserves = array();
						
						while($row = pg_fetch_array($reserverecord)){
						
										unset($reserve);
										$reserve->id = $row['id'];
										$reserve->rtype = $row['rtype'];
												
										$reserve->guest = $row['guest'];
										$reserve->name = $row['name'];
										
										array_push($reserves,$reserve);
									

						}
					
											
					 $Showmsg .='<table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table" style="margin-top: 15px;">
                    	<tr bgcolor="e74c3c" class="heading">
                        	<td class="top-td3">'.$lang_resource['RESERVATION'].'</td>
                        </tr>';
					
			if(sizeof($order->reserveQty->Table) != 0){			
						
                $Showmsg .='<tr bgcolor="#f7f7f7">
                        	<td style="text-align:center;">Table</td>
                        </tr>
                        <tr bgcolor="#ebebeb">
                        	<td>';
						foreach ($reserves as $a){	
						 if($a->rtype == 1){
							$Showmsg .='<div style="width: 25%;margin-bottom: 5px;">'.$a->name.'</div>
							<div class="tab-table">';
									$countg = $a->guest;
									for($countr =1; $countr<=$countg; $countr++){
									
									if(in_array($a->id.'_'.$countr,$order->reserveQty->Table)){
									 $Showmsg .='<div class="butt1">
                                    	<button></button>
									</div>';
                                    }else{
                                    $Showmsg .='<div class="butt2"><button></button></div>';
									}
									
									}
									
							$Showmsg .='</div>';
						 
						}
						}
                            $Showmsg .='</td>
                        </tr>';
			}
			if(sizeof($order->reserveQty->Room) != 0){			
						
                $Showmsg .='<tr bgcolor="#f7f7f7">
                        	<td style="text-align:center;">Room</td>
                        </tr>
                        <tr bgcolor="#ebebeb">
                        	<td>';
						foreach ($reserves as $a){	
						 if($a->rtype == 2){
							$Showmsg .='<div style="width: 25%;margin-bottom: 5px;">'.$a->name.'</div>
							<div class="tab-table">';
									$countg = $a->guest;
									for($countr =1; $countr<=$countg; $countr++){
									if(in_array($a->id.'_'.$countr,$order->reserveQty->Room)){
									 $Showmsg .='<div class="butt1">
                                    	<button></button>
									</div>';
                                    }else{
                                    $Showmsg .='<div class="butt2"><button></button></div>';
									}
									
									}
									
							$Showmsg .='</div>';
						 
						}
						}
                            $Showmsg .='</td>
                        </tr>';
			}
						 
			if(sizeof($order->reserveQty->Free) != 0){			
						
                $Showmsg .='<tr bgcolor="#f7f7f7">
                        	<td style="text-align:center;">Free</td>
                        </tr>
                        <tr bgcolor="#ebebeb">
                        	<td>';
						foreach ($reserves as $a){	
						 if($a->rtype == 3){
							$Showmsg .='<div style="width: 25%;margin-bottom: 5px;">'.$a->name.'</div>
							<div class="tab-table">';
									$countg = $a->guest;
									for($countr =1; $countr<=$countg; $countr++){
									if(in_array($a->id.'_'.$countr,$order->reserveQty->Free)){ 
									 $Showmsg .='<div class="butt1">
                                    	<button></button>
									</div>';
                                    }else{
                                    $Showmsg .='<div class="butt2"><button></button></div>';
									}
									
									}
									
							$Showmsg .='</div>';
						 
						}
						}
                            $Showmsg .='</td>
                        </tr>';
			}
					
					
					
						
                    $Showmsg .='</table>';
	
				pg_prepare($link,'sqlRTitleShow','SELECT name FROM w_reserve WHERE id =$1');
					
					if($order->reserveQty) {
						foreach($order->reserveQty as $value=>$keyval)
						{
								$rr=0;
								$tt=0;
								$ff=0;
								
								if($value == "Room") {
				
							 foreach($keyval as $rrr) {
									
									$room_data1 = explode("_",$rrr);
									
									$resultTileroom1 = pg_execute($link,'sqlRTitleShow',array($room_data1[0]));
									
									$titleroom1 = pg_fetch_array($resultTileroom1);
									
									
									
									$order->reserveQty->Room1[$rr] = $titleroom1['name']."_".$room_data1[1];
									
									$rr++;
									}
							}
								
								
							if($value == "Table") {
							
							 foreach($keyval as $tb) {
									
									$table_data1 = explode("_",$tb);
									
									$resultTileroom1 = pg_execute($link,'sqlRTitleShow',array($table_data1[0]));
									
									$titleroom1= pg_fetch_array($resultTileroom1);
									
									
									
									$order->reserveQty->Table1[$tt] = $titleroom1['name']."_".$table_data1[1];
									
									$tt++;
									}
							}
							
							if($value == "Free") {
				
									foreach($keyval as $fre) {
									
									$free_data1 = explode("_",$fre);
									
									$resultTilefree1 = pg_execute($link,'sqlRTitleShow',array($free_data1[0]));
									
									$titlefree1 = pg_fetch_array($resultTilefree1);
									
									$order->reserveQty->Free1[$ff] = $titlefree1['name']."_".$free_data1[1];
									
									$ff++;
									}
							}
							
							
						}
					}

					
				pg_prepare($link,'sqlroomshow','SELECT price FROM w_reserve_chart WHERE rtype=$1 and business =$2');
				$resultroom = pg_execute($link,'sqlroomshow',array(2,$order->business[0]->id));
				$rowroom = pg_fetch_array($resultroom);
				
				$order->roomprice = $rowroom['price'];		
				
				pg_prepare($link,'sqltableshow','SELECT price FROM w_reserve_chart WHERE rtype=$1 and business =$2');
				$resulttable = pg_execute($link,'sqltableshow',array(1,$order->business[0]->id));
				$rowtable = pg_fetch_array($resulttable);
				
				$order->tableprice = $rowtable['price'];		
			
				pg_prepare($link,'sqlfreeshow','SELECT price FROM w_reserve_chart WHERE rtype=$1 and business =$2');
				$resultfree = pg_execute($link,'sqlfreeshow',array(3,$order->business[0]->id));
				$rowfree = pg_fetch_array($resultfree);
				
				$order->freeprice = $rowfree['price'];	
				
						
			if(sizeof($order->reserveQty->Table) != 0){	
			$tableprice = sizeof($order->reserveQty->Table) * $order->tableprice ;	
			$tableprice = $tableprice;	
				 $Showmsg .='<table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table" style="margin-top: 15px;">
                    	<tr bgcolor="e74c3c" class="heading">
                        	<td class="top-td1">'.$lang_resource['FRONT_RESERVATION_TABLE'].'</td>
                            <td class="top-td1">'.$lang_resource['PRODUCT_POTIONS_QUANTITY'].'</td>
                            <td class="top-td1">'.$lang_resource['CONTROL_PANEL_BUSINESS_DISHES_PRICE_HEADER'].'</td>
                            
                        </tr>
                        <tr bgcolor="#f7f7f7">
                        	<td style="border-bottom:solid 2px #fff;padding:8px;">' . implode(", ",$order->reserveQty->Table1). '</td>
                            <td style="border-bottom:solid 2px #fff;padding:8px;">'.$lang_resource['FRONT_RESERVATION_TABLE'].' '.sizeof($order->reserveQty->Table).' X '.$order->tableprice.'</td>
                            <td style="border-bottom:solid 2px #fff;padding:8px;">'.$row23b["currency"].' '.$tableprice.'</td>
                        </tr>
                    </table>';
			 $total	= ($total + $tableprice);
			}
			
			
			if(sizeof($order->reserveQty->Room) != 0){	
			$roomprice = sizeof($order->reserveQty->Room) * $order->roomprice ;
			$roomprice = $roomprice;	
				 $Showmsg .='<table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table" style="margin-top: 15px;">
                    	<tr bgcolor="e74c3c" class="heading">
                        	<td class="top-td1">'.$lang_resource['FRONT_RESERVATION_ROOM'].'</td>
                            <td class="top-td1">'.$lang_resource['PRODUCT_POTIONS_QUANTITY'].'</td>
                            <td class="top-td1">'.$lang_resource['CONTROL_PANEL_BUSINESS_DISHES_PRICE_HEADER'].'</td>
                            
                        </tr>
                        <tr bgcolor="#f7f7f7">
                        	<td style="border-bottom:solid 2px #fff;padding:8px;">' . implode(", ",$order->reserveQty->Room1). '</td>
                            <td style="border-bottom:solid 2px #fff;padding:8px;">'.$lang_resource['FRONT_RESERVATION_TABLE'].' '.sizeof($order->reserveQty->Room).' X '.$order->roomprice.'</td>
                            <td style="border-bottom:solid 2px #fff;padding:8px;">'.$row23b["currency"].' '.$roomprice.'</td>
                        </tr>
                    </table>';
			 $total	= ($total + $roomprice);
			}
			
			if(sizeof($order->reserveQty->Free) != 0){	
			$freeprice = sizeof($order->reserveQty->Free) * $order->freeprice ;
			$freeprice = $freeprice;	
				 $Showmsg .='<table border="0" cellpadding="5" cellspacing="0" width="100%" class="detail-table" style="margin-top: 15px;">
                    	<tr bgcolor="e74c3c" class="heading">
                        	<td class="top-td1">'.$lang_resource['FRONT_RESERVATION_FREE'].'</td>
                            <td class="top-td1">'.$lang_resource['PRODUCT_POTIONS_QUANTITY'].'</td>
                            <td class="top-td1">'.$lang_resource['CONTROL_PANEL_BUSINESS_DISHES_PRICE_HEADER'].'</td>
                            
                        </tr>
                        <tr bgcolor="#f7f7f7">
                        	<td style="border-bottom:solid 2px #fff;padding:8px;">' . implode(", ",$order->reserveQty->Free1). '</td>
                            <td style="border-bottom:solid 2px #fff;padding:8px;">'.$lang_resource['FRONT_RESERVATION_TABLE'].' '.sizeof($order->reserveQty->Free).' X '.$order->freeprice.'</td>
                            <td style="border-bottom:solid 2px #fff;padding:8px;">'.$row23b["currency"].' '.$freeprice.'</td>
                        </tr>
                    </table>';
			 $total	= ($total + $freeprice);
			}
				
					
	}           
	
	if($order->reservestatus){  
                if(!$order->business[0]->dishes){
                    $deltype = $lang_resource['FRONT_MAIN_HOME_RESERVE'];
                }else{
                    $deltype = $lang_resource['FRONT_MAIN_HOME_ORDER_RESERVE'];
                }
            } 
			 
             $Showmsg .= '<table border="0" cellpadding="5" cellspacing="0" class="tab-total">';

            if($order->usedpointvalue !=""){
            	$Showmsg .= '<tr>
                    <td class="td1"><span>'.$lang_resource['PAID_POINT_TOTAL'].'</span></td>
                    <td class="td2">
                        <span class="sp2">
                            <img src="../../templates/images/total-border.png"/>
                        </span>
                        <span class="sp3">'.$row23b["currency"].'' .convert2Digit($order->usedpointvalue) . '</span>
                       
                    </td>
                </tr>';
            }
            


            $Showmsg .= '<tr>
                    <td class="td1"><span>'.$lang_resource['MOBILE_CHECKOUT_TOTAL'].'</span></td>
                    <td class="td2">
                        <span class="sp2">
                            <img src="../../templates/images/total-border.png"/>
                        </span>
                        <span class="sp3">'.$row23b["currency"].'' .convert2Digit($total) . '</span>
                        <span class="sp4">
                            <img src="../../templates/images/total-border.png"/>
                        </span>
                    </td>
                </tr>
            </table>
                    
            <div class="pay-method">
                
                <div><span><strong>'.$lang_resource['EXPORT_PAYMENT_METHOD'].'</strong></span> :'. $paymethod_paypal.' </div>';
            
			$Showmsg .= '<div style="padding-bottom:15px;"><span><strong>'.$lang_resource['FRONT_MAIN_EMAIL_DELIVERY_TYPE'].'</strong></span> :  ' . $deltype . '  </div>';	
		
                
                
            $Showmsg .= '</div>
                    
            <div class="app-table">
                
                <div class="apps">
                    <a href="#" style="cursor:pointer;"><img src="../../templates/images/apple-icon.png" alt=""/></a>
                </div>
                <div class="apps">
                    <a href="#" style="cursor:pointer;"><img src="../../templates/images/android-icon.png" alt=""/></a>
                </div>
                <div class="apps">
                    <a href="http://'.$_SERVER['HTTP_HOST'].'/mobile.php" style="cursor:pointer;"><img src="../../templates/images/mobile-icon.png" alt=""/></a>
                </div>
                
                
            </div>
            <div class="back-sec">
            	<a href="http://'.$_SERVER["HTTP_HOST"].'" style="text-decoration: none;"><button type="button" class=" back_btn">'.$lang_resource['PAYMENT_ORDER_BACK'].'</button></a>
            </div><!--back-sec-->
                    
            <div class="footer-table">
            </div>
                    
                
        </div>
        
    </div>


</body>
</html>';


unset($total);
$msg ='';
$msg .='<!doctype html>
<html>
    <head>
    <meta charset="utf-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Use the latest (edge) version of IE rendering engine -->
    <title>Ordering</title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
    <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->

    <!-- CSS Reset -->
    <style type="text/css">
/* What it does: Remove spaces around the email design added by some email clients. */
      /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
html,  body {
    margin: 0 !important;
    padding: 0 !important;
    height: 100% !important;
    width: 100% !important;
}
/* What it does: Stops email clients resizing small text. */
* {
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
}
/* What it does: Forces Outlook.com to display emails full width. */
.ExternalClass {
    width: 100%;
}
/* What is does: Centers email on Android 4.4 */
div[style*="margin: 16px 0"] {
    margin: 0 !important;
}
/* What it does: Stops Outlook from adding extra spacing to tables. */
table,  td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
}
/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
table {
    border-spacing: 0 !important;
    border-collapse: collapse !important;
    table-layout: fixed !important;
    margin: 0 auto !important;
}
table table table {
    table-layout: auto;
}
/* What it does: Uses a better rendering method when resizing images in IE. */
img {
    -ms-interpolation-mode: bicubic;
}
.yshortcuts a {
    border-bottom: none !important;
}
/* What it does: Another work-around for iOS meddling in triggered links. */
a[x-apple-data-detectors] {
    color: inherit !important;
}
</style>

    <!-- Progressive Enhancements -->
    <style type="text/css">
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid-centered {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* And center justify these ones. */
            .fluid-centered {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
        
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
            .center-on-narrow-left {
                text-align: left !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow-left {
                display: inline-block !important;
            }
            .center-on-narrow-right {
                text-align: right !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow-right {
                display: inline-block !important;
            }
                
        }

    </style>
    </head>';
    $b_id = $order->business[0]->id; // particular Business id
	pg_prepare($link,'sql5','SELECT * from w_business WHERE id=$1');
	$result23 = pg_execute($link,'sql5',array($b_id));
	$row23 = pg_fetch_array($result23);

	$deliverytime = $row23['deliverytime'];
	$pickuptime = $row23['pickuptime'];
	$bstreet = $row23['street'];
	$bcolony = $row23['colony'];
	$btel = $row23['tel'];
	$bcurrency = currency_symbol($row23['currency']);
    $msg .='<body bgcolor="#fff" width="100%" style="margin: 0;" yahoo="yahoo">
        <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;">
            <tr>
                <td>
                    <center style="width: 100%;">            

                        <!-- Email Header : BEGIN -->
                        <table align="center" width="600" class="email-container" style="border-bottom: 1px solid #cccccc">
                            <tr>
                                <td style="padding: 20px 0; text-align: center">
                                    <img src="'.$logo->secure_url.'" width="200" height="" alt="alt_text" border="0">
                                </td>
                            </tr>
                        </table>
                        <!-- Email Header : END --> 

                        <!-- Email Body : BEGIN -->
                        <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#ffffff" width="600" class="email-container">
                            <!-- 1 Column Text : BEGIN -->
                            <tr>
                                <td style="padding: 20px 40px 40px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                    <p style="color: #303030; font-size: 16px; margin-bottom: 26px;">'.$lang_resource['ORDER_EMAIL_TEMPLATE_HI'].''." ".''. $order->buyer->name.''." ".''.$order->buyer->lastname2.'</p>
                                    <p style="color: #ef1962; font-size: 20px; margin-bottom: 26px;">'.$lang_resource['EMAIL_TEMPLATE_ORDER_STATUS_HEADING'].'</p>
                                    <p style="color: #303030; font-size: 16px; margin-bottom: 26px;">'.$lang_resource['EMAIL_TEMPLATE_ORDER_STATUS_PENDING'].'</p>                         
                                    <!-- Button : Begin -->
                                    <table cellspacing="0" width="80%" cellpadding="0" border="0" align="center" style="margin: auto">
                                        <tr>
                                            <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                                                <a href="'.$siteurl.'" style="background: #ef1962; border: 15px solid #ef1962; padding: 0 10px;color: #ffffff; font-family: sans-serif; font-size: 16px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px;" class="button-a"> 
                                                <!--[if mso]>&nbsp;&nbsp;&nbsp;&nbsp;<![endif]-->'.$lang_resource['TRACK_MY_ORDER_NOW'].'<!--[if mso]>&nbsp;&nbsp;&nbsp;&nbsp;<![endif]--> 
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- 1 Column Text : END --> 

                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border:1px solid #d1d1d1;  ">

                                        <!-- Three Even Columns : BEGIN -->
                                        <tr>
                                            <td align="center" valign="top" style="padding: 10px;">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td width="33.33%" class="stack-column-center">
                                                            <table cellspacing="0" cellpadding="0" border="0">
                                                                
                                                                <tr>
                                                                    <td style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
                                                                        <p><b>'.$lang_resource['EMAIL_TEMPLATE_FROM'].'</b></p>
                                                                        <p style="font-size: 14px; color: #555555">'.$order->business[0]->name.'</p>
                                                                        <p style="font-size: 14px; color: #555555">'.$bstreet.' '." ".' '.$bcolony.'</p>
                                                                        <p style="font-size: 14px; color: #555555">'.$btel.'</p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                        <td width="33.33%" class="stack-column-center">
                                                            <table cellspacing="0" cellpadding="0" border="0">
                                                                <tr>
                                                                    <td style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow"> </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="33.33%" class="stack-column-center">
                                                            <table cellspacing="0" cellpadding="0" border="0">
                                                                <tr>
                                                                    <td style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 0 10px 10px; text-align: right;" class="center-on-narrow">    <p><b>'.$lang_resource['EMAIL_TEMPLATE_TO'].'</b></p>
                                                                        <p style="font-size: 14px; color: #555555">'.$order->buyer->name.''." ".''.$order->buyer->lastname2.'</p>
                                                                        <p style="font-size: 14px; color: #555555">'.$order->buyer->address.' '.$order->buyer->colony.'</p>
                                                                        <p style="font-size: 14px; color: #555555">'.$order->buyer->tel.'</p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- Three Even Columns : END --> 



                                        <tr>
                                            <td align="center" valign="top" style="padding: 10px;">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">';
$twilio_phone;
$twilio_enabled;
$twilio_order = "";
$business = $order->business[0];
$twilio_phone = $business->twiliophone;
$twilio_enabled = $business->acceptsms;	

pg_query($link, "DEALLOCATE ALL");
pg_prepare($link,'sql4'.$business->id,'SELECT buys from w_business WHERE id=$1');
$result2 = pg_execute($link,'sql4'.$business->id,array($business->id));
if (pg_num_rows($result2)==1){
	while($row2 = pg_fetch_array($result2)){
		pg_prepare($link,'sqls'.$business->id,'UPDATE w_business SET buys=$2 WHERE id=$1');
		pg_execute($link,'sqls'.$business->id,array($business->id,intval($row2['buys'])+1));
	}
}		

if (!empty($business->email) && $business->email!='null')
	array_push($emails,$business->email);

$total = 0;
$paymethod = ''; 
if((!isset($requestcollectionset))||($requestcollectionset==false)){ 	
	if($order->business[0]->dishes){

		foreach ($business->dishes as $dish){
			if($dish->options) {
				$productOptionHtml =  Margeslash($dish->options);  
			} else {
				$productOptionHtml ='';
			}                                           
                                                
                                                    $msg.='<tr>
                                                        <td width="16.5%" valign="top"><p style="color: #3c3c3c;font-size: 14px;padding: 0 10px 10px 10px;">'.$dish->quantity.' X</p></td>
                                                        <td width="56.66%"  valign="top">
                                                            <p style="color: #3c3c3c;font-size: 14px;">'.$dish->name.'</p> '.$productOptionHtml.'
                                                            
                                                            <p style="color: #939393; font-size: 13px; margin: 0; padding: 0">'.str_replace("%20", " ", ucfirst($dish->comments)) .'</p>                                                 
                                                        </td>
                                                        <td width="26.5%"  valign="top"><p style="color: #3c3c3c;font-size: 14px;text-align: right;padding: 0 20px 10px 0;">'.$lang_resource['Panel_Currency'].''.number_format($dish->total,2).'</p></td>
                                                    </tr>';
            $total = $total + $dish->total;
			$subtotal = number_format($total,2) ;
	     }
                                               $msg .=' </table>
                                            </td>
                                        </tr>'; 
	 }
}
                                                                            
                                       $msg .=' <tr>
                                        	<td valign="top" style="padding-top: 25px;">
                                        		<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top: 1px solid #cccccc; background: #f5f5f5;">
												  <tbody>';
												  
if($order->business[0]->dishes){

	if($order->buyer->taxtype == 1){
		$total = $total + $business->shipping + $order->tax;
		$taxstring = $lang_resource['FRONT_MAIN_EMAIL_TAX_NOT_INCLUDED'];
	}else{
		$total = $total + $business->shipping;
		$taxstring = $lang_resource['FRONT_MAIN_EMAIL_TAX_INCLUDED'];
    }
				
	$taxpercentage = $order->buyer->tax;
	$tipsprice = $order->buyer->tips;
	
	if ($order->buyer->tips > 0){
		$total = $total	+ $order->buyer->tips;
	}
			
	$total = $total;
				
	$deltype = $order->buyer->deliveryType;
	//subtotal
	$msg .='<tr>
				<td style="padding: 15px; color: #494949; font-size: 14px;">'.$lang_resource['SUBTOTAL'].'</td>
				<td style="padding: 15px; color: #494949; font-size: 14px;  text-align: right;">'.$lang_resource['Panel_Currency'].' '. $subtotal.'</td>
			</tr>';
	//subtotal
	
	//Shipping
	if ($business->shipping=='0.00')
		$shippingcaption = $lang_resource['FRONT_MAIN_HOME_DELIVERY'];
	else
		$shippingcaption = $lang_resource['FRONT_MAIN_HOME_DELIVERY_WITH_COST'];

	if($deltype != "pickup") {
	$msg .='<tr>
				<td style="padding: 15px; color: #494949; font-size: 14px;">'.$shippingcaption.''.str_replace("%20", " ", ucfirst($order->buyer->comments)) .'</td>
				<td style="padding: 15px; color: #494949; font-size: 14px;  text-align: right;">'.$lang_resource['Panel_Currency'].' '. number_format($business->shipping,2) .'</td>
			</tr>';
	}
	//Shipping
	
	//Service Fee
	if(isset($order->servicefeeTotal1)){
		if(($order->servicefee==null) ||($order->servicefee=='')){
			$order->servicefee=0;
		}
		$discountcaption = $lang_resource['SERVICE_FEE_V2'] ." (" .$order->servicefee ."%)";
	$msg .='<tr>
				<td style="padding: 15px; color: #494949; font-size: 14px; ">'.$discountcaption.'</td>
				<td style="padding: 15px; color: #494949; font-size: 14px;  text-align: right;">'.$lang_resource['Panel_Currency'].' '. number_format($order->servicefeeTotal1,2) .'</td>
			</tr>';
	}
	
	//Discount Section
	if(in_array('Discount Coupon', $checkoutfields)){	
		if((isset($order->discountcategory)) && (trim($order->discountprice) !='')){
			if($order->discounttype == 1)
				$discountcaption = $lang_resource['SHOPPING_DISCOUNT_TEXT'] ." (" .$order->discountrate ."%)";
			else if($order->discounttype == 2)
				$discountcaption = $lang_resource['SHOPPING_DISCOUNT_TEXT'] ;

			$total = $total - $order->discountprice;
			$total = $total;
	$msg .='<tr>
				<td style="padding: 15px; color: #494949; font-size: 14px; ">'.$discountcaption.''.ucfirst(strtolower($order->discountcomments)) .'</td>
				<td style="padding: 15px; color: #494949; font-size: 14px;  text-align: right;">'.$lang_resource['Panel_Currency'].' '. number_format($order->discountprice,2) .'</td>
			</tr>';
		}
	}
	//Discount Section
	
	//Tax
	if ($order->tax > 0){
	$msg .='<tr>
				<td style="padding: 15px; color: #494949; font-size: 14px; ">'.$lang_resource['Tax_V2'].' ('. $taxpercentage .'%)<br>'. ucfirst(strtolower($taxstring)).'</td>
				<td style="padding: 15px; color: #494949; font-size: 14px;  text-align: right;">'.$lang_resource['Panel_Currency'].' '. number_format($order->tax,2) .'</td>
			</tr>';
	}
	//Tax
	//Tip For the Driver
	if(in_array('Tip For The Driver', $checkoutfields)){			
		if ($order->buyer->tips > 0){
	$msg .='<tr>
				<td style="padding: 15px; color: #494949; font-size: 14px; ">'.$lang_resource['SHOPPING_FOURTH_TIP'].'</td>
				<td style="padding: 15px; color: #494949; font-size: 14px;  text-align: right;">'.$lang_resource['Panel_Currency'].' '. number_format($tipsprice,2) .'</td>
			</tr>';
		}
	}
	//Tip For the Driver
}
if($order->usedpointvalue !=""){
	$msg .='<tr>
				<td style="padding: 15px; color: #494949; font-size: 14px; ">'.$lang_resource['PAID_POINT_TOTAL'].'</td>
				<td style="padding: 15px; color: #494949; font-size: 14px;  text-align: right;">'.$row23b["currency"].' '. number_format($tipsprice,2) . '</td>
			</tr>';
	
}
	
	$msg .='<tr>
				<td style="padding: 15px; color: #494949; font-size: 22px;">'.$lang_resource['MOBILE_CHECKOUT_TOTAL'].'</td>
				<td style="padding: 15px; color: #ef1962; font-size: 22px; text-align: right;">'.$lang_resource['Panel_Currency'].'' .number_format($order->total,2). '</td>
			</tr>';
$msg .='</tbody>
	</table>
   </td>                                        	
 </tr>                                                     
  
 
 </table>
 </td>
 </tr>';

if($deltype == "pickup") {
    $deltypelang = $lang_resource['FRONT_MAIN_HOME_PICKUP'];
}else  if($deltype == "delivery"){
    $deltypelang = $lang_resource['FRONT_MAIN_HOME_DELIVERY_TEXT'] ;  
}

if($order->reservestatus){  
    if(!$order->business[0]->dishes){
        $deltypelang = $lang_resource['FRONT_MAIN_HOME_RESERVE'];
    }else{
        $deltypelang = $lang_resource['FRONT_MAIN_HOME_ORDER_RESERVE'];
    }
} 
if ($order->business[0]->paymethod->paypal==true) {
  if(isset($tx))
	{
     $paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYPAL']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE'].":" . $tx . ")";
	}
	else
	{
		 $paymethod = "Order Placed (Pending Payment)";
	}
}
if ($order->business[0]->paymethod->transactium==true) {
				if ($paymethod=='')
				   if ($paymentid=='Failure'){
					$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_NOT_TRANSACTIUM'];
					}else{
					$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_TRANSACTIUM']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";	
					}
					
					
			}
			
if ($order->business[0]->paymethod->pexpress==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYMENTEXPRESS']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";		
}

if ($order->business[0]->paymethod->maksekeskus==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_MAKSEKESKUS']." "."(".$lang_resource['ORDER_EMAIL_TEMPLATE_TRANSACTION_CODE']. $paymentid . ")";		
}

if ($order->business[0]->paymethod->voguepay==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_VOGUEPAY']." "."(" . $paymentid . ")";	
}

if ($order->business[0]->paymethod->skrill==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_SKRILL']." "."(" . $paymentid . ")";	
}

if ($order->business[0]->paymethod->payeezy==true){
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYEEZY']." "."(" . $paymentid . ")";	
}
if ($order->business[0]->paymethod->payu==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYU']." "."(" . $paymentid . ")";	
}
if ($order->business[0]->paymethod->stripe==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_STRIPE']." "."(" . $paymentid . ")";	
}
if ($order->business[0]->paymethod->btrans==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_BTRANS']." "."(" . $paymentid . ")";	
}
if ($order->business[0]->paymethod->bsa==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_BSA']." "."(" . $paymentid . ")";	
}
if ($order->business[0]->paymethod->azul==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['PAYMENT_GATEWAY_ALL_AZUL_PAYMENT_SUCCESS']." "."(" . $paymentid . ")";	
}
if ($order->business[0]->paymethod->quickpay==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_QUICKPAY']." "."(" . $paymentid . ")";	
}
if ($order->business[0]->paymethod->paynl==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_PAYNL']." "."(" . $paymentid . ")";	
}
if ($order->business[0]->paymethod->zaakpay==true) {
	if ($paymethod=='')
		$paymethod = $lang_resource['ORDER_EMAIL_TEMPLATE_PAID_VIA_ZAAKPAY']." "."(" . $paymentid . ")";	
}
if ($order->business[0]->paymethod->cash==true) {
	if ($paymethod=='')							
		$paymethod = $lang_resource['FRONT_MAIN_EMAIL_UPON_RECEIVING_PAY_CASH'];
}
if ($order->business[0]->paymethod->card==true) {
	if ($paymethod=='')							
		$paymethod = $lang_resource['CARD_DELIVERY'];
}
                            
                            
                            
                            


                           $msg .=' <!-- Three Even Columns : BEGIN -->
                            <tr>
                                <td align="center" valign="top">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-bottom: 1px solid #cccccc;">
                                        <tr>
                                            <td width="45%" class="stack-column">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    
                                                    <tr>
                                                        <td style="font-family: sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 25px 0 25px; text-align: left;" class="center-on-narrow"><b>'.$lang_resource['FRONT_MAIN_PAYMENT_METHOD'].' </b>'.$paymethod.'
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td width="9.99%" class="stack-column">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 0; text-align: right;" class="center-on-narrow"> </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="45%" class="stack-column">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td style="font-family: sans-serif; font-size: 14px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 25px 0 25px; text-align: right;" class="center-on-narrow"><b>'.$lang_resource['CONFIRMATION_DELIVERY_METHOD'].' :</b>'.$deltypelang.'
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- Three Even Columns : END -->

                            <!-- Three Even Columns : BEGIN -->
                            <tr>
                                <td align="center" valign="top" style="padding: 10px;">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td width="9.5%%" class="stack-column-center">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    
                                                    <tr>
                                                        <td style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow"></td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td width="80%" class="stack-column-center">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td style="font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 25px 0 25px; text-align: center;" class="center-on-narrow">'.$lang_resource['THANK_YOU_FOR_CHOOSING'].' '." ".''.$sitname.'</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="9.5%" class="stack-column-center">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 0 10px 10px; text-align: left;" class="center-on-narrow"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- Three Even Columns : END -->
                        </table>
                        <!-- Email Body : END --> 


                        <!-- Email Footer : BEGIN -->
                        <table align="center" width="600" class="email-container" style="background:#1d1d1d;">
                            <tr>
                                <td style="padding: 40px 10px;width: 100%;font-size: 15px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #ffffff;">'.$lang_resource['EMAIL_FOOTER_TEXT1'].' '." ".' '.$order->business[0]->name.' '.$lang_resource['EMAIL_PRINTER_STATUS_UPDATE_AT'].' '.$btel.', '.$lang_resource['EMAIL_FOOTER_TEXT2'].' '." ".'   <a href="" style="color:#fff;">'.$lang_resource['CONTACT_US_EMAIL'].'</a> '.$lang_resource['EMAIL_FOOTER_TEXT3'].' '." ".' '.$lang_resource['CONTACT_US_NO'].'.<br><br>
                                    <span class="mobile-link--footer">'.$lang_resource['ORDER_PROCESSED'].' '." ".' '.$id.'.</span> 
                                    <br>
                                    <span class="mobile-link--footer">© '.$curyear.' <a href="" style="color:#fff;">'." ".''.$sitname.'</a></span>                     
                                </td>
                            </tr>
                        </table>
                        <!-- Email Footer : END -->
                    </center>
                </td>
            </tr>
        </table>
    </body>
</html>';

//end order email
$b_id=$order->business[0]->id; // particular Business id
pg_prepare($link,'sql5','SELECT * from w_business WHERE id=$1');
$result23 = pg_execute($link,'sql5',array($b_id));
$row23 = pg_fetch_array($result23);
if ($row23['isimg']==1){ // Check image is upload for particular Business
$b_img='/panel/images/business/'.$row23['id'].'/original.jpg';
}else{
$b_img='/panel/images/business/dummy.jpg';	
}  
			  


///////////////PDF html


$msg_pdf = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title></title>';
//$msg_pdf.="<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>";
$msg_pdf.='<style type="text/css">
* { font-family: "Open Sans"; }
body{
	font-size:10px;
	
}

*{margin:0; padding:0;}
.clr{
	clear:both;
}
.wrapper{
	width:695px;
	height:auto;
	margin:0 auto;
}

.header{
	width:100%;
	margin:0 0 0px 0;
	display:inline-block;
}

.logo{
	float:left;
	margin:0 0 0 0;
	padding:0 0 0 0;
}

.info{
	float: right;
	margin: 0px 0px 0px 110px;
	width: 469px;
	padding:0 0 0 0;
	text-align:right;
	
}

.info p{
	font-size:12px;
	font-family: "Open Sans";
	
}
	

.bill_add{
	width:100%;
	height:auto;
	display:inline-block;
	margin:10px 0;
}
.bill_add table{
	border:1px solid #000;
}
.main_tbl{
	margin:10px 0px 0px 0px;
}
.main_tbl thead tr th{
	
	font-family: "Open Sans";
	margin:0px 0px 0px 0px;
	color:#000000;
	line-height:15px;
	border-bottom:1px solid #000000;
	border-top:1px solid #000000;
	border-left:1px solid #000000;
	padding:5px 10px;
}



.main_tbl tr td{
	
	font-family: "Open Sans";
	margin:0px 0px 0px 0px;
	color:#000000;
	line-height:15px;
	border-bottom:1px solid #000000;
	border-left:1px solid #000000;
	padding:5px 15px 5px 15px;
}






.con_box{
	width:100%;
	height:auto;
	display:inline-block;
	margin:0px 0;
	font-size:10px;
	font-family: "Open Sans";
}





.addres_box{
	width:100%;
	height:auto;
	display:inline-block;
	margin:10px 0;
}

.addres_left{
	width:300px;
	height:auto;
	float:left;
	display:block;
	margin:0 20px 0 0;
}

.addres_right{
	width:auto;
	height:auto;
	float:right;
	display:block;
}
.imagediv{
	margin:12px;
	padding: 3px 0 23px 4px;
}
ul {
	list-style: none;
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	width: 100%;
	float:left;
	
	}
li{
font-size: 12px;
color: #333;
padding-left: 20px;
line-height: 17px;
}
 h4{
margin: 5px 0px 0px 0px;
padding: 0px 0px 0px 0px;
font-size: 12px;
color: #e74c3c;
text-transform: uppercase;
}
.options{
	width:100%;
	float:left;
	margin: 7px 0px 0px 0px;
padding: 0px 0px 0px 5px;
}
</style>



</head>

<body>

<!--wrapper-->
<div class="wrapper">

<div class="header">
<table>
<tr>
<td>
<div class="logo">

                                	
<img  width="87" height="78" alt="logo" src="http://'.$_SERVER['HTTP_HOST'].$b_img.'" >

</div>
</td>
<td style="padding:12px;">
<div class="info">
<p style="height:42px;"><a href="http://'.$_SERVER['HTTP_HOST'].'">';
					if($_SESSION['scriptid']=='0' || $_SESSION['scriptid']=='')
					{
                     $msg_pdf .=   '<img src="http://'.$_SERVER['HTTP_HOST'].'/panel/images/logo/1/normal.jpg" width="300px" alt="logo"/>';
					}
					else
					{
						 $msg_pdf .=   '<img src="http://'.$_SERVER['HTTP_HOST'].'/panel/images/logo/1/'.$_SESSION['scriptid'].'/normal.jpg" width="300px" alt="logo"/>';
					}
                   $msg_pdf .=  '</a></p>
<p style="height:20px;"><span class="imagediv"><a href="#"><img src="http://'.$_SERVER['HTTP_HOST'].'/images/email/23931.png" width="15"></a><a href="#"><img src="http://'.$_SERVER['HTTP_HOST'].'/images/email/facebook-512.gif" width="20"></a><a href="#"><img src="http://'.$_SERVER['HTTP_HOST'].'/images/email/rss-icon.png" width="18"></a></span></p>
<p>'.$lang_resource['ORDER_PDF_TITLE']. $id .'</p>';
 if((!isset($requestcollectionset))||($requestcollectionset==false)){
if ($order->preorder == 'true') {
						  //Time selection settings. 
							   $str='';
							$time_format=$rowtimeformat['value'];
							if($time_format==12){
								
									$str='PM';
									if($order->preordertimehh<12){
										$str='AM';
									}
									$order->preordertimehh=floor($order->preordertimehh%12);
									
									 if (intval($order->preordertimehh) < 10) {
										$order->preordertimehh = "0".$order->preordertimehh;
									}
									if (intval($order->preordertimemm) < 10) {
										$order->preordertimemm = "0".$order->preordertimemm;
									}
							}
	$msg_pdf .='<p >'.$lang_resource['V3_ORDER_DELIVERY_DATE'].' & '.$lang_resource['RESPONSIVE_ORDER_EMAIL_TIME'].' : '.$order->buyer->deliverydate.'  '.$order->preordertimehh.':'.$order->preordertimemm.'</p>';
}
 }
$msg_pdf .='<p>'.$lang_resource['FRONT_MAIN_EMAIL_ORDER_TYPE'].' : '. $deltypelang . '</p>

<p><a style="text-decoration:none;color:#000;" href="http://'.$_SERVER['HTTP_HOST'].'">'.$tr_o.'</a></p>

</div>

</td>
</tr>
<tr><td colspan="2" style=" border-bottom: 1px solid #000000;"></td>
</tr>

</table>

<div class="clr"></div>
</div>
<br />
<div class="con_box">
<table width="100%" cellspacing="5" cellpadding="0" style="font-size:12px;font-family: "Open Sans";">';
 if((!isset($requestcollectionset))||($requestcollectionset==false)){  
  $checkoutfields = $order->buyer->checkoutfields;
	if(in_array('Name', $checkoutfields)){
		$msg_pdf .='<tr><td width="30%">'.$lang_resource['LOGIN_CREATE_NAME'].'</td>
		<td width="1%">:</td>
		<td width="31%">'. ucfirst(str_replace("%20", " ", $order->buyer->name)) . '</td></tr>';
	}
	if(in_array('Email', $checkoutfields)){
		$msg_pdf .='<tr><td width="6%">'.$lang_resource['LOGIN_CREATE_EMAIL'].'</td>
		<td width="3%">:</td>
		<td width="29%">'. strtolower($order->buyer->email) . '</td>';
		$msg_pdf .='</tr>';
	}
	
	if(in_array('Full Address', $checkoutfields)){
		$addr = str_replace("%20", " ", $order->buyer->address);
		$addrs = str_replace("%2C", " ", $addr);
		$msg_pdf .='<tr><td>'.$lang_resource['FRONT_MAIN_EMAIL_HOME_ADDRESS'].'</td>
		<td>:</td>
		<td>'. ucfirst($addrs) . '</td></tr>';
	}
	if(in_array('Phone', $checkoutfields)){
		$msg_pdf .='<tr><td>'.$lang_resource['Phone_V2'].'</td>
		<td>:</td>
		<td>'. $order->buyer->tel . '</td>';  
		$msg_pdf .='</tr>';
	}
  
	if(in_array('City', $checkoutfields)){	
		$msg_pdf .='<tr><td>'.$lang_resource['EXPORT_CITY'].'</td>
		<td>:</td>
		<td>'. $order->buyer->cityname . '</td></tr>';
	}
	
	if(in_array('Zipcode', $checkoutfields)){
		$msg_pdf .='<tr><td>'.$lang_resource['MOBILE_FRONT_VISUALS_ZIP'].'</td>
		<td>:</td>
		<td>'. $order->buyer->zipcode . '</td>';
		$msg_pdf .='</tr>';
	}
	
	if(in_array('APT/Suit', $checkoutfields)){	
		$msg_pdf .='<tr><td>'.$lang_resource['MOBILE_FRONT_VISUALS_APT'].'</td>
		<td>:</td>
		<td>'. $order->buyer->api . '</td></tr>';
	}
 }else{
	$msg_pdf .='<tr>
    <td width="30%">'.$lang_resource['LOGIN_CREATE_NAME'].'</td>
    <td width="1%">:</td>
    <td width="31%">'. ucfirst($requestcollectiondata[0]["customer_name"]) . '</td>
    <td width="6%">'.$lang_resource['MOBILE_FRONT_VISUALS_CITY'].'</td>
    <td width="3%">:</td>
    <td width="29%">'. $requestcollectiondata[0]["customer_town"]  . '</td>
 
  </tr>
  <tr>
    <td>'.$lang_resource['FRONT_MAIN_EMAIL_HOME_ADDRESS'].'</td>
    <td>:</td>
    <td>'.ucfirst($requestcollectiondata[0]["customer_address1"].','.$requestcollectiondata[0]["customer_address2"]) . '</td>
    <td>'.$lang_resource['Phone_V2'].'</td>
    <td>:</td>
    <td>'. $requestcollectiondata[0]["customer_contactno"] . '</td>
    
  </tr>';
 }

$msg_pdf .='<tr><td colspan="6" style=" border-bottom: 1px solid #000000;"></td>
</tr>
</table>
</div>
<br />
<div class="bill_add">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="main_tbl" style="font-size: 12px; font-family: "Open Sans";">
                <thead>
                  <tr>
                    <th>'.$lang_resource['CONTROL_PANEL_ORDERS_EDIT_PAY_METHOD_TABLE_ITEM'].'</th>';
					
					   if((!isset($requestcollectionset))||($requestcollectionset==false)){
                    $msg_pdf .='<th>'.$lang_resource['PRODUCT_POTIONS_QUANTITY'].'</th>
					<th>'.$lang_resource['SHOPPING_NO_OF_PERSON'].'</th>
                    <th >'.$lang_resource['FRONT_MAIN_EMAIL_OPTIONS'].'</th>
                    <th>'.$lang_resource['FRONT_MAIN_EMAIL_COMMENT'].'</th>';
					   }else{
						    $msg_pdf .='<th>&nbsp;</th>
                    <th >&nbsp;</th>
                    <th>&nbsp;</th>';
					   }
                   $msg_pdf .=' <th  style="text-align:right;">'.$lang_resource['FRONT_MAIN_EMAIL_RATE'].'</th>
                  </tr>
              </thead>';
			     if((!isset($requestcollectionset))||($requestcollectionset==false)){
			  if(!empty($business->dishes)){
				  foreach ($business->dishes as $dish){
					  if($dish->options) {
						 $productOptionHtml =  Margeslash($dish->options);  
						} else {
						$productOptionHtml ='';
					}
					
					
					
					  $msg_pdf .='<tr style="page-break-after: always;">
						<td>' . $dish->name . '</td>
						<td>'.$dish->quantity.'</td>
						<td>'.$dish->nofperson.'</td>
						<td>' . $productOptionHtml .'</td>
						<td>' . str_replace("%20", " ", ucfirst($dish->comments)) . '</td>
						<td  style="text-align:right;" >'.$row23b["currency"].' '. convert2Digit($dish->total) . '</td>
					  </tr>';
				  }
			  }
                  if($order->tax) {
                  $msg_pdf .='<tr>
                    <td>'.$taxstring.'</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
					<td>&nbsp;</td>
                    <td  style="text-align:right;" >'.$row23b["currency"].' '.convert2Digit($order->tax).'</td>
                  </tr>';
				  }
				}else{
					
					   $msg_pdf .='<tr>
                    <td>'.$lang_resource['REQUEST_COLLECTION_DELIVERY_FEE'].'</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
					<td>&nbsp;</td>
                    <td  style="text-align:right;" >'.$row23b["currency"].' ' .$requestcollectiondata[0]["deliveryprice"]. '</td>
                  </tr>';
				  $total=0;
				   $total = $total +$requestcollectiondata[0]["deliveryprice"];
					 
					 
				}
		  if((!isset($requestcollectionset))||($requestcollectionset==false)){		  
		if ($particular_business->shipping == '0.00')
			$shippingcaption = $lang_resource['FRONT_MAIN_HOME_DELIVERY'];
		else
			$shippingcaption = $lang_resource['FRONT_MAIN_HOME_DELIVERY'];

				if($deltype != "pickup") {	

   $msg_pdf .= '<tr>
                    <td>' . $shippingcaption . '</td>
                    <td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
                    <td>' . ucfirst(strtolower($particular_business->comments)) . '</td>
                    <td style="text-align:right;">'.$row23b["currency"].' '. convert2Digit($particular_business->shipping ). '</td>
                </tr>';
				       }		
		  }
		  
				  /*service fee section */
		  if(isset($order->servicefeeTotal1))
		  {
			 if(($order->servicefee==null) ||($order->servicefee=='')){
				 $order->servicefee=0;
			 }
			   $discountcaption = $lang_resource['SERVICE_FEE_V2'] ." (" .$order->servicefee ."%)";
			  
		
			$msg_pdf .='<tr>
					  <td >' . $discountcaption . '</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					   <td>&nbsp;</td>
					  <td   style="text-align:right;"> '.$row23b["currency"].' '. convert2Digit($order->servicefeeTotal1) . '</td>
				      </tr>';
				  
		  }
		  /*service fee section */  
		  
		   /*tip for the driver section */  
		  if(isset($order->buyer->tips))
		  {
			 if(($order->buyer->tips==null) ||($order->buyer->tips=='')){
				 $order->buyer->tips=0;
			 }
			 
			$msg_pdf .='<tr>
					  <td >' . $lang_resource['SHOPPING_FOURTH_TIP'] . '</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					   <td>&nbsp;</td>
					  <td   style="text-align:right;"> '.$row23b["currency"].' '. convert2Digit($order->buyer->tips) . '</td>
				      </tr>';
				  
		  }
		  /*tip for the driver section */   
		  
		  
		    if((!isset($requestcollectionset))||($requestcollectionset==false)){
		  /*discount rate*/
		  if(in_array('Discount Coupon', $checkoutfields)){
		   if((isset($order->discountcategory)) && (trim($order->discountprice) !=''))
		  {
			  if($order->discounttype == 1)
			  $discountcaption = $lang_resource['SHOPPING_DISCOUNT_TEXT'] ." (" .$order->discountrate ."%)";
			  else if($order->discounttype == 2)
			  $discountcaption = $lang_resource['SHOPPING_DISCOUNT_TEXT'] ;
			  
			//  $total = $total - $order->discountprice;
			//  $total = number_format($total, 2);
			
			$msg_pdf .='<tr>
					  <td >' . $discountcaption . '</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					   <td>'. ucfirst(strtolower($order->discountcomments)) . '</td>
					  <td   style="text-align:right;"> '. convert2Digit($order->discountprice) . '</td>
				      </tr>';
				  
		  }
		}
		  
		  /*end discount rate8*/  
		  
		  //delivery comments
		  $msg_pdf .='<tr>
					  <td >' . $shippingcaption . '</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					   <td>'. ucfirst(strtolower($order->buyer->comments)). '</td>';
					   if($deltype != "pickup") {	
					   
					  $msg_pdf .='<td   style="text-align:right;">'.$row23b["currency"].' '. convert2Digit($business->shipping) . '</td>';
					   }
					   else
					   {
						 $msg_pdf .='<td   style="text-align:right;">0.00</td>';   
						   
					   }
					   
				      $msg_pdf .='</tr>';     
				  
			}

			if($order->usedpointvalue !=""){
				$msg_pdf .= '<tr>
				<td colspan="5" align="right";><strong>'.$lang_resource['PAID_POINT_TOTAL'].'</strong></td>
				<td style="text-align:right;"><strong> '.$row23b["currency"].''.convert2Digit($order->usedpointvalue).'</strong></td>
				</tr>';
			}



              $msg_pdf .='<tr>
                    <td colspan="5" align="right";><strong>'.$lang_resource['MOBILE_CHECKOUT_TOTAL'].'</strong></td>
                    <td style="text-align:right;"><strong> '.$row23b["currency"].''.convert2Digit($order->total).'</strong></td>
                  </tr>
            </table>
</div>
<br />
<!--addres_box-->
<div class="addres_box">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="footer_tbl" style="font-size: 11px; font-family: "Open Sans";">
              <tr>
                <td width="84%"><p><strong>'.$lang_resource['MOBILE_MENU_LIST_PAYMENT_METHOD'] .':</strong>'. $paymethod.'</p></td>
                <td width="6%"><a href="#"><img src="https://cdn0.iconfinder.com/data/icons/glyph_set/128/apple.png" width="35"></a>
                </td>
                 <td width="5%"><a href="#"><img src="https://lh3.ggpht.com/lAjUqEGdt1ZOOcKMRALBOVW3sj1NgDy6OiKo60fK6Sp4BjWPZvwv9jOi4K4NZrJATtE=w300" width="30"></a>
                </td>
                <td width="5%">
                        <a href="#"><img src="http://static.wixstatic.com/media/686a9a_26d87b5b7f454636aa3d2ffe4e8e53ce.png" width="28"></a>
                  
                </td>
              </tr>
            </table>
<div class="clr"></div>
</div>
<!--addres_box end-->

<div class="clr"></div>
</div>
<!--wrapper end-->
</body>
</html>
';

/////////////////PDF html ends


function Margeslash($text){
	
	$res = explode("_@_",$text);
	
	$options_record = Array();
	foreach($res as $po){
		$poption = explode("@u@",$po);
		$a=new stdClass();
		$a->optionheader=$poption[0];
		$a->optionchoice=$poption[1];
		if($poption[0]!=""){	
			array_push($options_record,$a);
		}
	}
	$lasti =0;
	$html ='';
	foreach($options_record as $each_record){
	if($lasti == 0 ) {
	$html .='<p style="color: #939393; font-size: 13px; margin: 0; padding: 0">'.$each_record->optionheader.'</p>';
	$html .= '<p style="color: #939393; font-size: 13px; margin: 0; padding: 0">'.$each_record->optionchoice.'</p>';
	
	}
	else if($previousHeader == $each_record->optionheader ) {
		$html .= '<p style="color: #939393; font-size: 13px; margin: 0; padding: 0">'.$each_record->optionchoice.'</p>';
	}
	else if($previousHeader != $each_record->optionheader) {
		$html .='<p style="color: #939393; font-size: 13px; margin: 0; padding: 0">'.$each_record->optionheader.'</p>';
		$html .= '<p style="color: #939393; font-size: 13px; margin: 0; padding: 0">'.$each_record->optionchoice.'</p>';
		
		}
	$previousHeader = $each_record->optionheader;
	$lasti++;
	}
	return $html;
}

?>
